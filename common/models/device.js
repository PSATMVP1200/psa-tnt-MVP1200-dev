/* eslint-disable max-len */
'use strict';

const _ = require('underscore'),
  async = require('async'),
  loopback = require('loopback'),
  ruleTnt = require('../lib/rules-tnt');

let app;

module.exports = function(Device) {
  Device.on('attached', function(a) {
    app = a;
  });

  Device.remoteMethod('clearFakeWarings', {
    isStatic: true,
    description: 'Removes warning states that have been unproperly reported',
    returns: {type: 'object', root: true},
    http: {verb: 'post'}
  });

  Device.clearFakeWarings = function(cb) {
    const fixedContainers = [], toFixGeoPos = [];

    Device.find({}, (err, devices) => {
      if (err) {
        return cb(err);
      }
      async.each(devices, (device, cb) => {
        device.geoPositions((err, gps) => {
          if (err) {
            return cb(err);
          }
          if (!Array.isArray(gps) || gps.length === 0) {
            return cb();
          }

          console.log('Dealing with: %s', device.id);
          let i = 1;
          while (i < gps.length - 1) {
            const pp = gps[i - 1];
            if (pp.status.marker === 'green') {
              let j = 0;
              while ((i + j) < gps.length && (gps[i + j].status.marker === 'orange') && (gps[i + j].siteId === pp.siteId)) {
                console.log('- It is: %s', gps[i + j].status.description);
                toFixGeoPos.push(gps[i + j]);
                j++;
              }
            }
            i++;
          }
          return cb();
        });
      }, err => {
        console.log(toFixGeoPos);
        if (err) {
          return cb(err);
        }
        async.map(toFixGeoPos, (toFix, cb) => {
          toFix.updateAttribute('status', {marker: 'green', description: 'OK'}, cb);
        }, cb);
      });
    });
  };

  Device.remoteMethod('reset', {
    isStatic: false,
    description: 'Resets the last & before last sites in traffic for a device',
    http: {verb: 'post'}
  });

  Device.prototype.reset = function(cb) {
    console.log('[Device.reset]');
    let device = this;
    device.currentGeoPosition.isReset = true;
    device.geoPositions.findById(device.currentGeoPosition.id, (err, geoPosition) => {
      if (err) {
        return cb(err);
      }
      geoPosition.isReset = true;
      async.parallel([
        cb => {
          device.save({'validate': false}, cb);
        },
        cb => {
          geoPosition.save({'validate': false}, cb);
        }
      ], cb);
    });
  };

  Device.remoteMethod('statusHistory', {
    isStatic: false,
    description: 'Get the status change history for a given device',
    accepts: {arg: 'allStatuses', type: 'string'},
    returns: {type: 'object', root: true},
    http: {verb: 'get'}
  });

  Device.prototype.statusHistory = function(allStatuses, cb) {
    const device = this;
    device.geoPositions.find({'order': 'timestamp DESC'}, (err, geoLocations) => {
      if (err) {
        return cb(err);
      }
      let currentStatusMarker, currentSite;
      let allGeoLocations;
      if (allStatuses !== 'true') {
        allGeoLocations = _.filter(geoLocations, (geoLocation) => {
          if (currentSite && (currentSite === geoLocation.siteId) && (geoLocation.status.marker === currentStatusMarker)) {
            return false;
          } else if (!currentSite && (geoLocation.status.marker === currentStatusMarker)) {
            return false;
          }
          currentSite = geoLocation.siteId;
          currentStatusMarker = geoLocation.status.marker;
          return true;
        });
      } else {
        allGeoLocations = geoLocations;
      }

      const filteredMinimalGeoLocations = _.map(allGeoLocations, g => _.pick(g, 'status', 'timestamp', 'siteId', 'geopoint', 'radiusInMeters', 'correctionType', 'source'));

      // Artificially create a new "quiet" status
      _.each(geoLocations, (geoLocation) => {
        if (geoLocation.quietTransition === 'to quiet') {
          console.log(geoLocation.status);
          filteredMinimalGeoLocations.push({
            status: {
              marker: 'grey',
              description: 'Container is marked as quiet'
            },
            'timestamp': geoLocation.timestamp,
            'siteId': geoLocation.siteId
          });
        } else if (geoLocation.quietTransition === 'to normal') {
          console.log(geoLocation.status);
          filteredMinimalGeoLocations.push({
            status: {
              marker: 'grey',
              description: 'Container is no longer quiet'
            },
            'timestamp': geoLocation.timestamp,
            'siteId': geoLocation.siteId
          });
        }
      });
      return cb(null, _.sortBy(filteredMinimalGeoLocations, 'timestamp').reverse());
    });
  };

  Device.remoteMethod('submitEvent', {
    isStatic: true,
    accepts: {
      'arg': 'data',
      'type': 'object',
      'http': {'source': 'body'}
    },
    'http': {'verb': 'post'}
  });

  Device.submitEvent = function(param, cb) {
    // TODO Data Type in Swagger does not show properly
    const deviceId = param.deviceId,
      lat = param.lat, lng = param.lng,
      wifiMac = param.wifiMac, batteryLevel = param.batteryLevel,
      inDate = new Date(param.date);
    const date = isNaN(inDate) ? new Date() : inDate;

    let geoLocation, attribute;
    if (typeof lat === 'number' && typeof lng === 'number') {
      geoLocation = {
        'timestamp': date,
        'geopoint': new loopback.GeoPoint({'lat': lat, 'lng': lng}),
        'radiusInMeters': param.radiusInMeters ? param.radiusInMeters : 5 // 5 meters is a good precision
      };
    }
    attribute = {'timestamp': date};
    if (wifiMac) {
      attribute.wifiMac = wifiMac;
    }
    if (typeof batteryLevel === 'number') {
      attribute.batteryLevel = batteryLevel;
    }
    if (_.keys(attribute).length <= 1) {
      attribute = null;
    }
    app.models.Device.findById(deviceId, {'include': 'container'}, (err, device) => {
      if (err) {
        return cb(err);
      }
      if (!device) {
        return cb(new Error('No device found for id=' + deviceId));
      }
      device.recordGeoPosition(device.container().containerTypeId, geoLocation, cb);
    });
  };

  Device.remoteMethod('destroyAll', {
    isStatic: true,
    description: 'Delete all matching records',
    accessType: 'WRITE',
    accepts: {arg: 'where', type: 'object', description: 'filter.where object'},
    http: {verb: 'del', path: '/'}
  });

  Device.remoteMethod('updateBadGPS', {
    isStatic: true,
    description: 'Update Bad GPS geolocation',
    accessType: 'WRITE',
    accepts: {arg: 'where', type: 'object', description: 'filter.where object'},
    http: {verb: 'get', path: '/updateBadGPS'}
  });

  Device.remoteMethod('updateBadRadius', {
    isStatic: true,
    description: 'Update Bad Radius geolocation',
    accessType: 'WRITE',
    accepts: {arg: 'where', type: 'object', description: 'filter.where object'},
    http: {verb: 'get', path: '/updateBadRadius'}
  });

  // This is a non-geo pos event (only record attributes)
  Device.prototype.recordAttributes = function(attributes, cb) {
    const device = this;
    if (attributes) {
      console.log('[Device.processAttributes:%s] Saving attributes', this.id);
      device.attributes.create(attributes, (err) => {
        if (err) {
          return cb(err);
        }
        device.updateAttribute('currentAttribute', attributes, {'validate': false}, cb);
      });
    } else {
      return cb();
    }
  };

  Device.prototype.recordGeoPosition = function(containerType, geoPosition, cb) {
    const device = this, date = geoPosition.timestamp;

    if (!geoPosition) {
      console.warn('[Device.recordGeoPosition:%s] No geo position and no site found, will neither record any position nor query rules engine', device.id);
      return cb(null);
    }

    let brRequest = {
      'containerType': containerType,
      'currentTrafficDate': date
    };

    // Perform site match (geo only)
    let site;
    let foundSites = app.models.Site.findCloseSites(geoPosition.geopoint, geoPosition.radiusInMeters, containerType);
    if (foundSites && Array.isArray(foundSites) && foundSites.length > 0) {
      if (foundSites.length > 1) {
        console.warn('[Device.recordGeoPosition:%s] /!\\ More than one site is found, this is not currently supported. Taking closest one', device.id);
      }
      site = _.last(foundSites);
      console.log('[Device.recordGeoPosition:%s] Site found from geo position: %s', device.id, site.name);
    } else {
      console.log('[Device.recordGeoPosition:%s] No site found from geo position', device.id);
    }
    if (site) {
      console.log('[Device.recordGeoPosition:%s] Site was computed, will replace geo position if it was provided', device.id);
      Object.assign(geoPosition, {
        'geopoint': site.geopoint,
        'siteId': site.name,
        'correctionType': 'site matching',
        'radiusInMeters': 0,
        'staticTime': 0
      });
      brRequest.currentContainerSite = site.name;
    }

    let geoPositionLastInTraffic;
    async.waterfall([
      cb => {
        // Find last site in traffic
        console.log('[Device.recordGeoPosition:%s] Look for last site in traffic', device.id);
        if (device.currentGeoPosition && device.currentGeoPosition.isReset) {
          console.log('[Device.recordGeoPosition] Reset is required');
          return cb(null, null);
        } else {
          getLastGeoPosInTraffic(device.id, site ? site.name : null, cb);
        }
      },
      (_geoPositionLastInTraffic, cb) => {
        geoPositionLastInTraffic = _geoPositionLastInTraffic;
        console.log('[Device.recordGeoPosition:%s] Last geo position in traffic: %j', device.id, geoPositionLastInTraffic);
        // Filter static positions if needed
        console.log('[Device.recordGeoPosition:%s] Will filter static positions', device.id);
        const staticTimeInMinutes = getStaticTimeInMins(geoPosition, device.currentGeoPosition);
        console.log('[Device.recordGeoPosition:%s] Static idle in mins: %j', device.id, staticTimeInMinutes);

        if (geoPositionLastInTraffic) {
          brRequest.lastSiteInTrafficType = geoPositionLastInTraffic.site().type;
          brRequest.lastSiteInTraffic = geoPositionLastInTraffic.site().name;

          if (site &&
            device.currentGeoPosition.status.marker === 'green' &&
            device.currentGeoPosition.isSiteInTraffic &&
            (device.currentGeoPosition.siteId === site.name)) { // Device is started only if it leaves site
            console.log('[Device.recordGeoPosition:%s] Skipping start traffic date (on site & same as previous)', device.id);
          } else {
            brRequest.startingTrafficDate = geoPositionLastInTraffic.timestamp;
          }
        }

        if (typeof staticTimeInMinutes === 'number') {
          brRequest.containerStaticTimeInMinutes = staticTimeInMinutes;
        }
        return processRules(brRequest, device.id, cb);
      },
      (brResponse, cb) => {
        console.log('[Device.recordGeoPosition:%s] Will now save geo position based on rules response: %j', device.id, brResponse);
        // calculate if we are static. Then remove the current position from the list if status is unchanged (will be replaced by the new calculated one)
        const newGeoPosition = filterStaticPositions(geoPosition, device.currentGeoPosition);
        if (newGeoPosition !== null &&
          device.currentGeoPosition.status.marker === brResponse.status.marker && // Keep state changes
          !device.currentGeoPosition.quietTransition // Keep moments where device becomes/is no longer quiet
        ) {
          console.log('[Device.recordGeoPosition] Will delete previous geo position');
          geoPosition = newGeoPosition;
          device.geoPositions.destroy(device.currentGeoPosition.id, (err, order) => {
            if (err) {
              console.error('[Device.recordGeoPosition:%s] Error while removing geo position', device.id, err.message);
            }
          });
        } else {
          console.log('[Device.recordGeoPosition:%s] Will NOT delete previous geoPosition', device.id);
        }
        // Save position upon response
        geoPosition.status = brResponse.status;
        if (brResponse.isCurrentSiteInTraffic) {
          geoPosition.isSiteInTraffic = true;
        }
        // Save container's current status & geo position
        device.container().updateAttributes({'currentStatus': brResponse.status}, (err) => {
          if (err) {
            console.warn('[Device.recordGeoPosition:%s] Unable to update attributes (%j)', device.id, err);
            return cb(err);
          }
          device.geoPositions.create(geoPosition, (err, _geoPosition) => {
            if (err) {
              console.warn('[Device.recordGeoPosition:%s] Unable to update attributes (%j)', device.id, err);
              return cb(err);
            }
            let deviceAttributes = {'currentGeoPosition': _geoPosition};
            device.updateAttributes(deviceAttributes, (err) => {
              if (err) {
                console.warn('[Device.recordGeoPosition:%s] Unable to update attributes (%j)', device.id, err);
                return cb(err);
              }
              if (brResponse.isCurrentSiteInTraffic) {
                // We're on site: update sites
                updateSites(device, site.name, cb);
              } else if (device.currentSiteInTrafficId) {
                // We're off site: update sites
                updateSites(device, null, cb);
              } else {
                return cb(null, null);
              }
            });
          });
        });
      }
    ], cb);
  };

  function updateSites(device, siteId, cb) {
    if (device.currentSiteInTrafficId === siteId) {
      return cb();
    }
    let attributesToUpdate = {};
    if (device.currentSiteInTrafficId) {
      // There was a current site: shift it all
      if (device.lastSiteInTrafficId) { // last becomes beforeLast
        attributesToUpdate.beforeLastSiteInTrafficId = device.lastSiteInTrafficId;
      }
      attributesToUpdate.lastSiteInTrafficId = device.currentSiteInTrafficId;
    }
    attributesToUpdate.currentSiteInTrafficId = siteId; // record current
    device.updateAttributes(attributesToUpdate, cb);
  }

  function getLastGeoPosInTraffic(deviceId, siteId, cb) {
    app.models.GeoPosition.findOne({
      'order': 'timestamp DESC',
      'where': {
        'and': [
          {'isSiteInTraffic': true},
          {'deviceId': deviceId},
          {'siteId': {'neq': siteId}}
        ]
      },
      'include': 'site'
    }, cb);
  }

  function calculateBarycentre(currentGeoPosition, newGeoPosition) {
    if (currentGeoPosition != null && newGeoPosition != null &&
      currentGeoPosition.geopoint != null && currentGeoPosition.geopoint.lat != null && currentGeoPosition.geopoint.lng != null && currentGeoPosition.radiusInMeters != null &&
      newGeoPosition.geopoint != null && newGeoPosition.geopoint.lat != null && newGeoPosition.geopoint.lng != null && newGeoPosition.radiusInMeters != null) {
      const newlat = ((currentGeoPosition.geopoint.lat * currentGeoPosition.weight) + newGeoPosition.geopoint.lat) / (currentGeoPosition.weight + 1);
      const newlng = ((currentGeoPosition.geopoint.lng * currentGeoPosition.weight) + newGeoPosition.geopoint.lng) / (currentGeoPosition.weight + 1);
      const newGeoPoint = new loopback.GeoPoint({'lat': newlat, 'lng': newlng});

      return newGeoPoint;
    } else {
      return null;
    }
  }

  function filterStaticPositions(newGeoPosition, currentGeoPosition) {
    let calculatedGeoPosition = null;
    if (areInSamePosition(newGeoPosition, currentGeoPosition)) {
      console.log('[Device.filterStaticPositions] New position is at the same position as previous one');
      // The new position is in the radius we need to recalculate the barycentre
      const barycentre = calculateBarycentre(currentGeoPosition, newGeoPosition);
      const staticTimeInMinutes = Math.floor((Math.abs(currentGeoPosition.timestamp - newGeoPosition.timestamp) / 1000) / 60);
      if (barycentre) {
        calculatedGeoPosition = newGeoPosition;
        calculatedGeoPosition.correctionType = 'barycentre';
        calculatedGeoPosition.weight = currentGeoPosition.weight + 1;
        calculatedGeoPosition.geopoint = barycentre;
        calculatedGeoPosition.radiusInMeters = (currentGeoPosition.radiusInMeters + newGeoPosition.radiusInMeters) / 2;
        calculatedGeoPosition.staticTime = currentGeoPosition.staticTime + staticTimeInMinutes;
        console.log('[Device.filterStaticPositionsAndFindStaticTimeInMins] : New position after calculated barycentre %j', calculatedGeoPosition);
      } else {
        console.warn('[Device.filterStaticPositionsAndFindStaticTimeInMins] : Not Able to calculate barycentre  %j %j', currentGeoPosition, newGeoPosition);
      }
    } else {
      console.log('[Device.filterStaticPositions] New position is NOT at the same position as previous one');
    }
    return calculatedGeoPosition;
  }

  function getStaticTimeInMins(newGeoPosition, currentGeoPosition) {
    let staticTimeInMinutes = 0;

    if (areInSamePosition(newGeoPosition, currentGeoPosition)) {
      staticTimeInMinutes = Math.floor((Math.abs(currentGeoPosition.timestamp - newGeoPosition.timestamp) / 1000) / 60);
      if (typeof currentGeoPosition.staticTime === 'number') {
        staticTimeInMinutes += currentGeoPosition.staticTime;
      }
    }
    return staticTimeInMinutes;
  }

  function areInSamePosition(newGeoPosition, currentGeoPosition) {
    if (newGeoPosition != null && currentGeoPosition != null && newGeoPosition.geopoint != null && currentGeoPosition.geopoint != null) {
      let distanceBetweenPositions = currentGeoPosition.geopoint.distanceTo(newGeoPosition.geopoint, {'type': 'meters'});
      console.log('[Device.areInSamePosition] : Distance Between Positions  ', distanceBetweenPositions);
      console.log('[Device.areInSamePosition] : Radiuses  %s %s', currentGeoPosition.radiusInMeters, newGeoPosition.radiusInMeters);

      let calculatedGeoPosition = null;
      if ((distanceBetweenPositions <= currentGeoPosition.radiusInMeters) ||                   // If new position is really near the previous (don't care about the new radius)
        (
          (newGeoPosition.radiusInMeters < app._internalConfig.MAX_ACCEPTABLE_RADIUS) &&                 // If The new radius is Acceptable
          (distanceBetweenPositions <= currentGeoPosition.radiusInMeters + newGeoPosition.radiusInMeters) // And the distance is between the radius sum
        )
      ) {
        return true;
      }
      return false;
    } else {
      return false;
    }
  }

  function processRules(brRequest, deviceId, cb) {
    console.log('[Device:processRules:%s] Rules request:', deviceId);
    console.log(brRequest);

    // FAKE BEGIN
    if (0 === 1) {
      return cb(null, {
        'isCurrentSiteInTraffic': !!brRequest.currentContainerSite,
        'status': {
          'marker': 'green',
          'description': 'fake'
        }
      });
    }
    // FAKE END

    ruleTnt.invokeRuleset(brRequest, (err, brResponse) => {
      if (err) {
        console.error('[Device.recordEvent] Error while invoking rules (%s), event will be discarded', err.message);
        Device.app.models.RulesAudit.create({
          deviceId: deviceId,
          date: new Date(),
          request: brRequest,
          error: err
        });

        return cb(err);
      }
      Device.app.models.RulesAudit.create({
        deviceId: deviceId,
        date: new Date(),
        request: brRequest,
        response: brResponse
      });
      return cb(null, brResponse);
    });
  }
};
