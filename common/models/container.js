'use strict';

const _ = require('underscore'),
  async = require('async'),
  moment = require('moment');

module.exports = function(Container) {
  Container.trends = function(containerTypes, forDate, cb) {
    const now = new Date();
    if (!forDate) {
      console.log('[Container:trends] No forDate provided, using 24 hours ago');
      forDate = new Date(now.getTime() - 86400 * 1000);
    }
    Container.find({
      'where': {
        'containerTypeId': {
          'inq': containerTypes
        }
      },
      'include': 'device'
    }, (err, allContainers) => {
      if (err) {
        return cb(err);
      }
      console.log('[Container:trends] Found %d containers', allContainers.length);

      const lastCounts = {
        'red': 0,
        'orange': 0,
        'green': 0,
        'blue': 0
      };
      async.each(allContainers, (container, cb) => {
        if (!container.device()) {
          console.warn('Container %s has no device !', container.id);
          return cb(null, null);
        }
        container.device().geoPositions.findOne({
          'where': {
            'timestamp': {
              'gte': forDate
            }
          }
        }, (err, foundGeoPosition) => {
          if (foundGeoPosition) {
            if (err) {
              return cb(err);
            }
            if (foundGeoPosition.quietTransition === 'to quiet') {
              lastCounts.gray++;
            } else {
              if (typeof lastCounts[foundGeoPosition.status.marker] === 'number') {
                lastCounts[foundGeoPosition.status.marker]++;
              }
            }
            return cb(null, foundGeoPosition.status.marker);
          } else {
            return cb(null);
          }
        });
      }, (err) => {
        Container.countStatuses(containerTypes, (err, currentCounts) => {
          if (err) {
            return cb(err);
          }
          return cb(null, _.mapObject(currentCounts, (val, key) => {
            return val - lastCounts[key];
          }));
        });
      });
    });
  };

  Container.countStatuses = function(containerTypes, cb) {
    let response = {};
    async.map(['green', 'blue', 'orange', 'red'], (marker, cb) => {
      countByContainerTypes({'and': [{'currentStatus.marker': marker}, {'isQuiet': {'neq': true}}]}, containerTypes, (err, count) => {
        if (err) {
          return cb(err);
        }
        response[marker] = count;
        return cb(null, null);
      });
    }, (err) => {
      if (err) {
        return cb(err);
      }
      cb(null, response);
    });
  };

  Container.countQuiet = function(containerTypes, cb) {
    console.log('[Container] countQuiet(%j)', containerTypes);
    countByContainerTypes({'isQuiet': 'true'}, containerTypes, cb);
  };

  Container.countLowBattery = function(containerTypes, cb) {
    console.log('[Container] countLowBattery(%j)', containerTypes);
    countByContainerTypes({'isLowBattery': 'true'}, containerTypes, cb);
  };

  Container.remoteMethod('destroyAll', {
    isStatic: true,
    description: 'Delete all matching records',
    accessType: 'WRITE',
    accepts: {arg: 'where', type: 'object', description: 'filter.where object'},
    http: {verb: 'del', path: '/'}
  });

  Container.remoteMethod('countStatuses', {
    'isStatic': true,
    'description': 'Returns the count of containers by status',
    'accepts': {
      'arg': 'containerTypes', 'type': 'array', 'description': 'The array of container types'
    },
    'returns': {'type': 'array', 'root': true},
    'http': {'verb': 'get'}
  });

  Container.remoteMethod('trends', {
    'isStatic': true,
    'description': 'Returns trend for containers status',
    'accepts': [
      {'arg': 'containerTypes', 'type': 'array', 'description': 'The array of container types'},
      {'arg': 'forDate', 'type': 'date', 'description': 'The comparing date for which trend is requested'}
    ],
    'returns': {'type': 'object', 'root': true},
    'http': {'verb': 'get'}
  });

  Container.remoteMethod('countQuiet', {
    'isStatic': true,
    'description': 'Returns the count of containers that are quiet',
    'accepts': {
      'arg': 'containerTypes', 'type': 'array', 'description': 'The array of container types'
    },
    'returns': {'type': 'array', 'root': true},
    'http': {'verb': 'get'}
  });

  Container.remoteMethod('countLowBattery', {
    'isStatic': true,
    'description': 'Returns the count of containers that are in low battery',
    'accepts': {
      'arg': 'containerTypes', 'type': 'array', 'description': 'The array of container types'
    },
    'returns': {'type': 'array', 'root': true},
    'http': {'verb': 'get'}
  });

  Container.remoteMethod('transitData', {
    'isStatic': true,
    'description': 'Returns the transit data',
    'returns': {'type': 'object', 'root': true},
    'http': {'verb': 'get'}
  });

  Container.transitData = function(cb) {
    const retVal = {};

    const addContainerTypeToRetval = containerType => {
      if (!retVal[containerType]) {
        retVal[containerType] = [];
      }
    };

    const addTransitData = (containerType, fromSite, toSite, startDate, endDate) => {
      const existingData = retVal[containerType].find(data => {
        return (data.fromSite === fromSite) && (data.toSite === toSite);
      });
      const startEnd = {
        startDate: startDate.getTime(),
        endDate: endDate.getTime()
      };
      if (existingData) {
        existingData.data.push(startEnd);
      } else {
        retVal[containerType].push({
          fromSite: fromSite,
          toSite: toSite,
          data: [startEnd]
        });
      }
    };

    const parseGeoPositions = (containerType, geoPositions) => {
      let fromSite, startDate, transitData = [];
      geoPositions.forEach(gp => {
        if (gp.siteId && gp.isSiteInTraffic && fromSite !== gp.siteId) {
          if (fromSite && startDate) {
            addTransitData(containerType, fromSite, gp.siteId, startDate, gp.timestamp);
          }
          startDate = gp.timestamp;
          fromSite = gp.siteId;
        }
      });
    };

    Container.app.models.Device.find({
      include: ['geoPositions', 'container']
    }, (err, devices) => {
      if (err) {
        return cb(err);
      }
      if (!devices || !Array.isArray(devices) || devices.length === 0) {
        return cb(null, []);
      }
      devices.forEach(device => {
        if (!device.container()) {
          console.error('/!/ Missing container for %s', device.id);
          return;
        }
        const containerType = device.container().containerTypeId;
        addContainerTypeToRetval(containerType);
        parseGeoPositions(containerType, device.geoPositions());
      });

      return cb(null, retVal);
    });
  }

  function countByContainerTypes(predicate, containerTypes, cb) {
    Container.count({
      'and':
        [predicate, {'containerTypeId': {'inq': containerTypes}}]
    }, cb);
  }
};
