'use strict';

const _ = require('underscore'),
  async = require('async');

module.exports = function(Site) {
  let app;

  Site.on('attached', function(a) {
    app = a;
  });

  Site.findCloseSites = function(geopoint, radiusInMeters, containerType) {
    if (typeof radiusInMeters !== 'number') {
      console.error('[Site:findCloseSites] Radius must be a number');
      return [];
    }
    console.log('[Site:findCloseSites] (%j), radius: %d', geopoint, radiusInMeters);
    const nonOrderedMatchingSites = [];
    _.each(app._cacheData.allSites, (site) => {
      const distanceInMeters = geopoint.distanceTo(site.geopoint, {'type': 'meters'});
      if (distanceInMeters < (radiusInMeters + site.radiusInMeters) && site.isMVP /*&& Array.isArray(site.containers) && site.containers.indexOf(containerType) >= 0*/) {
        nonOrderedMatchingSites.push({
          'site': site,
          'distanceInMeters': distanceInMeters
        });
      }
    });
    console.log('[Site:findCloseSites] Found %d site(s) for (%s,%s) in %d site(s)',
      nonOrderedMatchingSites.length,
      geopoint.lat, geopoint.lng,
      app._cacheData.allSites.length);
    return _.pluck(_.sortBy(nonOrderedMatchingSites, 'distanceInMeters'), 'site');
  };

  Site.remoteMethod('destroyAll', {
    isStatic: true,
    description: 'Delete all matching records',
    accessType: 'WRITE',
    accepts: {arg: 'where', type: 'object', description: 'filter.where object'},
    http: {verb: 'del', path: '/'}
  });

  Site.findByWifiMac = function(wifiMacArr, cb) {
    let foundSite;
    if (Array.isArray(wifiMacArr) && wifiMacArr.length > 0) {
      async.detectSeries(wifiMacArr, (wifiMac, cb) => {
        if (!wifiMac || typeof(wifiMac) !== 'string' || wifiMac.length !== 17) {
          return cb(null, false);
        }
        app.models.WifiAccessPoint.find({
          'where': {
            'macAddress': {
              'regexp': '^' + wifiMac.substring(0, 14)
            }
          },
          'include': 'site'
        }, (err, wifiAccessPoint) => {
          if (err) {
            return cb(new Error('Unable to read access points: ' + err.message));
          }
          if (wifiAccessPoint && Array.isArray(wifiAccessPoint) && wifiAccessPoint.length > 0) {
            if (wifiAccessPoint.length > 1) {
              console.warn('[Site:findByWifiMac] Found %j for %s - THIS IS AN ERROR', wifiAccessPoint, wifiMac);
            }
            foundSite = wifiAccessPoint[0].site();
          }
          return cb(null, !!foundSite);
        });
      }, (err, wifiMac) => {
        if (err) {
          return cb(new Error('Unable to read access points: ' + err.message));
        }
        if (foundSite) {
          console.log('[Site:findByWifiMac] Wifi MAC@ %s corresponds to site %s', wifiMac, foundSite.name);
        } else {
          console.log('[Site:findByWifiMac] No site found for %j', wifiMacArr);
        }
        return cb(null, foundSite);
      });
    } else {
      return cb(null, undefined);
    }
  };
};
