'use strict';

const async = require('async');

module.exports = function(ContainerType) {
  ContainerType.remoteMethod('destroyAll', {
    isStatic: true,
    description: 'Delete all matching records',
    accessType: 'WRITE',
    accepts: {arg: 'where', type: 'object', description: 'filter.where object'},
    http: {verb: 'del', path: '/'}
  });

  ContainerType.remoteMethod('provisionDevices', {
    isStatic: false,
    description: 'Provision containers & devices',
    accepts: {arg: 'deviceList', type: 'string', description: 'Comma-separated values of Sigfox device IDs'},
    returns: {arg: 'result', type: 'object'},
    http: {verb: 'post'}
  });

  ContainerType.prototype.provisionDevices = function(deviceList, cb) {
    const containerType = this, app = ContainerType.app, re = /^[0-9A-Fa-f]+$/;
    async.mapSeries(deviceList.split(','), (deviceId, cb) => {
      deviceId = deviceId.replace(/\b0+/g, '').toUpperCase();
      if (deviceId.length === 0 || !re.test(deviceId)) {
        return cb(null, 'Incorrect ID: ' + deviceId);
      }

      app.models.Device.findById(deviceId, {include: 'container'}, (err, device) => {
        if (err) {
          return cb(err);
        }
        if (device) {
          let msg;
          if (device.container() && device.container().containerTypeId) {
            msg = 'Device already exists: ' + deviceId + ' (as type ' + device.container().containerTypeId + ')';
          } else {
            msg = 'Device already exists: ' + deviceId + ' (unknown container type)';
          }
          return cb(null, msg);
        }
        containerType.containers.create({}, (err, container) => {
          if (err) {
            return cb(err);
          }
          if (!container) {
            return cb(new Error('Unable to create container: ' + deviceId));
          }
          container.device.create({
            id: deviceId,
            type: 'sigfox-auth-device',
            simulate: false
          }, (err, device) => {
            if (err) {
              return cb(err);
            }
            if (!device) {
              return cb(new Error('Unable to create device: ' + deviceId));
            }
            return cb(null, 'Device OK: ' + deviceId);
          });
        });
      });
    }, cb);
  };
};
