/* eslint-disable max-len */
'use strict';
const
  _ = require('underscore'),
  async = require('async'),
  iot = require('../lib/iot-worker'),
  loopback = require('loopback'),
  wifiLocator = require('../lib/wifi-locator');

module.exports = function(TechnicalData) {

  TechnicalData.remoteMethod('findMacsForPSASites', {
    isStatic: true,
    returns: {type: 'object', root: true},
    http: {verb: 'get'}
  });

  TechnicalData.findMacsForPSASites = function(cb) {
    const limit = null, Site = TechnicalData.app.models.Site;
    // 1. Find all seq numbers where radius exists
    TechnicalData.find({
      'where': {
        'receivedData.radius': {
          'neq': null
        }
      },
      'limit': limit
    }, (err, td) => {
      if (err) {
        return cb(err);
      }
      // 2. Find all seqNumbers where device is on site
      const mapped = _.compact(_.map(td, v => {
        const lat = parseFloat(v.receivedData.lat),
          lng = parseFloat(v.receivedData.lng),
          radius = parseFloat(v.receivedData.radius),
          seqNumber = v.receivedData.seqNumber,
          device = v.receivedData.device;

        const sites = Site.findCloseSites(new loopback.GeoPoint({'lat': lat, 'lng': lng}), radius);
        if (Array.isArray(sites) && sites.length > 0) {
          const site = sites[0];
          return {
            site: site.name,
            seqNumber: seqNumber,
            device: device
          };
        } else {
          return null;
        }
      }));

      // 3. Now find
      async.mapLimit(mapped, 200, (v, cb) => {
        TechnicalData.find({
          'where': {
            'and': [{
              'receivedData.seqNumber': {
                'eq': v.seqNumber
              }
            }, {
              'receivedData.data': {
                'neq': null
              }
            }, {
              'receivedData.device': {
                'eq': v.device
              }
            }]
          }
        }, (err, tds) => {
          if (err) {
            return cb(err);
          }
          if (Array.isArray(tds) && tds.length > 0) {
            if (tds.length > 1) {
              console.warn('MORE THAN ONE');
            }
            if (tds[0].receivedData.data.length < 18) {
              return cb(null, null);
            }
            let mac = tds[0].receivedData.data.substring(6);
            mac = mac.substring(0, 2) + ':' + mac.substring(2, 4) + ':' +
              mac.substring(4, 6) + ':' + mac.substring(6, 8) + ':' +
              mac.substring(8, 10) + ':' + mac.substring(10, 12);
            if (mac === '00:00:00:00:00:00') {
              return cb(null, null);
            }
            return cb(null, {
              mac: mac,
              site: v.site
            });
          } else {
            return cb(null, null);
          }
        });
      }, (err, tds) => {
        if (err) {
          return cb(err);
        }
        tds = _.compact(tds);

        const sites = {};
        tds.forEach(v => {
          if (sites[v.site]) {
            if (sites[v.site].indexOf(v.mac) < 0) {
              sites[v.site].push(v.mac);
            }
          } else {
            sites[v.site] = [v.mac];
          }
        });

        _.keys(sites).forEach(c => {
          console.log();
          console.log(c);
          _.each(sites[c], v => {
            console.log(v);
          });
        });
        //console.log(sites);
        return cb();
      });
    });
  };

  TechnicalData.remoteMethod('parseWifi', {
    isStatic: true,
    description: 'Parse all wifi data',
    returns: {type: 'object', root: true},
    http: {verb: 'get'}
  });

  TechnicalData.parseWifi = function(cb) {
    console.log('[technical-data.parseWifi] Macs: %d', macs.length);

    const wifis = [], resolved = [], limit = 1000000000;

    const retval = {};

    TechnicalData.find({
      'where': {'receivedData.data': {'neq': null}},
      'limit': limit
    }, (err, values) => {
      if (err) {
        return cb(err);
      }
      values = _.filter(values, v => {
        return v.receivedData.data.length >= 18;
      });
      retval['Messages containing MAC@'] = values.length;
      values = _.map(values, v => {
        v = v.receivedData.data.substring(6);
        return v.substring(0, 2) + ':' + v.substring(2, 4) + ':' +
          v.substring(4, 6) + ':' + v.substring(6, 8) + ':' +
          v.substring(8, 10) + ':' + v.substring(10, 12);
      });

      values = _.without(values, '00:00:00:00:00:00');

      // Find mostly used
      let mostlyUsed = _.countBy(values, v => {
        return v;
      });
      let found = _.map(_.keys(mostlyUsed), v => {
        return {
          'mac': v,
          'times': mostlyUsed[v]
        };
      });
      found = _.sortBy(found, 'times');
      found = _.last(found, 15);
      retval['Mostly used'] = found;

      values = _.uniq(values);
      retval['Unique MAC@'] = values.length;

      const resolvedValues = _.filter(values, v => {
        return !!_.findWhere(macs, {'macAddress': v});
      });

      // Hack to test mac
      //values.push('2c:3e:cf:54:de:70');

      retval['Resolved MAC@ (based on PSA MAC@)'] = resolvedValues.length;

      // Now let's resolve using Wifi locator
      async.mapLimit(values, 50, (v, cb) => {
        wifiLocator.singleWifiGeoLoc(v, cb);
      }, (err, res) => {
        if (err) {
          return cb(err);
        }
        res = _.compact(res);
        retval['Resolved MAC@ (based on mylnikov)'] = res.length;
        console.log(retval);
        return cb(err, retval);
      });
    });
  };

  TechnicalData.remoteMethod('destroyAll', {
    isStatic: true,
    description: 'Delete all matching records',
    accessType: 'WRITE',
    accepts: {arg: 'where', type: 'object', description: 'filter.where object'},
    http: {verb: 'del', path: '/'}
  });

  TechnicalData.remoteMethod('replayTechnicalData', {
    isStatic: true,
    description: 'Replay All Technical Data',
    accessType: 'WRITE',
    accepts: {arg: 'where', type: 'object', description: 'filter.where object'},
    http: {verb: 'get', path: '/replayTechnicalData'}
  });

  TechnicalData.replayTechnicalData = function(wh, cb) {
//    let where = '{"where": {"or": [{"geopoint.lat": 49}, {"geopoint.lat": 47}, {"geopoint.lat": 48}]}}';
    let where;
    if (wh)
      where = wh;
    else
      where = JSON.parse('{"limit": 10}');
    TechnicalData.find(where, (err, technicalDatas) => {
      if (err) {
        console.error('[TechnicalData:replayTechnicalData] Error', err);
        return cb(err);
      } else {
        let discarded = 0;
        let current = 0;
        console.log('[TechnicalData:replayTechnicalData] Found %s Technical Data', technicalDatas.length);
        let functions = [];
        technicalDatas.forEach(function(technicalData) {
          functions.push((cb) => {
            current++;
//            console.log('[TechnicalData:replayTechnicalData] ', technicalData);
            console.log('[TechnicalData:replayTechnicalData] replay of %s event. discarded %s', current, discarded);
            if (technicalData.receivedData.device) {
              console.log('[TechnicalData:replayTechnicalData] replay of %s event. discarded %s', current, discarded);
              iot.receivedReplayDeviceEvent('sigfox-auth-device', technicalData.receivedData.device, '', '', JSON.stringify(technicalData.receivedData), technicalData.id, technicalData.receivedDate, cb);
            } else {
              discarded++;
              console.error('NO DEVICE ID ##### DISCARD', technicalData.id);
              cb();
            }

            //   deviceState.updateAttributes({isValidGeopoint: false}, (err, _deviceState) => {
            //     if (err) {
            //       console.error('[device:updateBadGPS] Error while Updating Device State : ', err);
            //       return cb(err);
            //     } else {
            //       console.log('[device:updateBadGPS] ', _deviceState.isValidGeopoint);
            //     }
            //   });
          });
        });
        if (technicalDatas.length == 0)
          return cb();
        async.series(functions, function(err, result) {
          if (err)
            console.error('[TechnicalData:replayTechnicalData] Error : ', err);
          console.log('[TechnicalData:replayTechnicalData] replay of %s events. discarded %s', current, discarded);
//          cb(err, null);
        });
        cb();
      }
    });
  };
};
