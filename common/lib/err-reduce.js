'use strict';

const _ = require('underscore');

module.exports = (err) => {
  if (Array.isArray(err)) {
    return _.reduce(err, (memo, singleErr) => {
      return memo + (memo === '' ? '' : ' / ') + getObjMessage(singleErr);
    }, '');
  } else {
    return getObjMessage(err);
  }
};

function getObjMessage(err) {
  if (err.message) {
    return err.message;
  } else {
    return '' + err;
  }
}
