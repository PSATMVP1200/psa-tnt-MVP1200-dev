const WIFI_GEOLOC_BASE_URL = 'https://api.mylnikov.org/geolocation/wifi?v1.1';

const _ = require('underscore'),
  async = require('async'),
  loopback = require('loopback'),
  request = require('request');

function singleWifiGeoLoc(macAddress, cb) {
  if (typeof cb !== 'function') {
    return;
  }

  if (typeof macAddress === 'string') {
    return _queryApi(WIFI_GEOLOC_BASE_URL + '&bssid=' + macAddress, cb);
  } else if (Array.isArray(macAddress)) {
    let foundWifiLoc = {};
    async.detect(macAddress, (mac, cb) => {
      if (typeof mac === 'string') {
        _queryApi(WIFI_GEOLOC_BASE_URL + '&bssid=' + mac, (err, wifiLoc) => {
          if (err) {
            return cb(err);
          }
          foundWifiLoc[mac] = wifiLoc;
          return cb(null, !!wifiLoc);
        });
      } else {
        return cb(null, false);
      }
    }, (err, mac) => {
      let retVal;
      if (mac) {
        retVal = foundWifiLoc[mac];
      } else {
        retVal = null;
      }
      return cb(err, retVal);
    });
  } else {
    return cb(new Error('Unsupported type for macAddress (only string & array of strings is accepted)'));
  }
}

function _queryApi(queryString, cb) {
  request(queryString, (err, res) => {
    if (err) {
      return cb(err);
    }
    let data;
    try {
      data = parseResponse(res);
    } catch (e) {
      return cb(e);
    }

    let retval = null;
    if (typeof data.lat === 'number' &&
      typeof data.lon === 'number' &&
      typeof data.range === 'number') {
      retval = {
        geopoint: new loopback.GeoPoint({'lat': parseFloat(data.lat), 'lng': parseFloat(data.lon)}),
        'radiusInMeters': data.range
      };
    }
    return cb(null, retval);
  });
}

function parseResponse(res) {
  // Nothing
  if (!res || !res.statusCode) {
    throw new Error('No data returned');
  }

  // An error code
  if (res.statusCode < 200 || res.statusCode >= 400) {
    if (!res.statusMessage) {
      throw new Error('Error: ' + res.statusCode);
    }
    throw new Error(res.statusMessage);
  }

  // No error
  if (res.body) {
    try {
      let jsonData = JSON.parse(res.body);
      if (jsonData.data) {
        return jsonData.data;
      } else {
        throw new Error('Missing data in body');
      }
    } catch (e) {
      throw new Error('Malformed body: ' + e.message);
    }
  } else {
    throw new Error('Empty body');
  }
}

module.exports = {
  'singleWifiGeoLoc': singleWifiGeoLoc
};
