'use strict';

const ODM_TRAFFIC_SERVICE_URL = '/Track_and_Trace_Rules/CalculateContainerStatus';

const _ = require('underscore'),
  app = require('../../server/server'),
  request = require('request');

function invokeRuleset(req, cb) {
  console.log('[rules-tnt:invokeRuleset] Invoking');
  /*
  const executionRestUrl = app._internalConfig.ODM_CREDENTIALS.executionRestUrl,
    username = app._internalConfig.ODM_CREDENTIALS.user,
    password = app._internalConfig.ODM_CREDENTIALS.password;
  */

  const executionRestUrl = "https://brsv2-d54ebb34.eu-gb.bluemix.net/DecisionService/rest",
    username = "resAdmin",
    password = "1hruc8eardz6s";

  const options = {
    'method': 'POST',
    'json': req,
    'uri': executionRestUrl + ODM_TRAFFIC_SERVICE_URL,
    'headers': {
      'Authorization': 'Basic ' + new Buffer(username + ':' + password).toString('base64')
    }
  };
  request(options, function(error, response, body) {

    if (error) {
      console.error('[rules-tnt:invokeRuleset] request failed: %s', error.message);
      return cb(error);
    }
    let data;
    try {
      data = _parseResponse(response);
    } catch (e) {
      return cb(e);
    }
    return cb(null, data);
  });
}

function _parseResponse(res) {
  // Nothing
  if (!res || !res.statusCode) {
    throw new Error('No data returned');
  }

  // An error code
  if (res.statusCode < 200 || res.statusCode >= 400) {
    if (!res.statusMessage) {
      throw new Error('Error: ' + res.statusCode);
    }
    throw new Error(res.statusMessage);
  }

  // No error
  if (res.body) {
    try {
      if (_.isObject(res.body))
        return res.body;
      let jsonData = JSON.parse(res.body);
      if (jsonData.data) {
        return jsonData.data;
      } else {
        throw new Error('Missing data in body');
      }
    } catch (e) {
      throw new Error('Malformed body: ' + e.message);
    }
  } else {
    throw new Error('Empty body');
  }
}

module.exports = {
  'invokeRuleset': invokeRuleset
};
