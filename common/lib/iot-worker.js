/* eslint-disable max-len */
'use strict';

const _ = require('underscore'),
  async = require('async'),
  Client = require('ibmiotf'),
  loopback = require('loopback'),
  wifiLocator = require('../lib/wifi-locator');

const msgQ = {};

const maxSingleV1Delay = 11 * 60 * 1000; // After this delay, a single message is sent regardless of its pair
setInterval(() => { // Our "garbage collector"
  // Check if there are pending messages, force send
  const now = new Date();
  _.keys(msgQ).forEach(v => {
    if (now.getTime() - msgQ[v].firstReveivedDate.getTime() > maxSingleV1Delay) {
      // It should be sent!
      console.log('[iot-worker.gc] Force sending unpaired message for %s', msgQ[v].device.id);
      send(v);
    }
  });
}, 2 * 60 * 1000);

let app, // Needs to be initialized here as this module is imported from server.js
  TechnicalData, Device, WifiAccessPoint, Site;

function start(credentials) {
  app = require('../../server/server');
  TechnicalData = app.models.TechnicalData;
  Device = app.models.Device;
  WifiAccessPoint = app.models.WifiAccessPoint;
  Site = app.models.Site;

  // Start listening to IoT
  if (credentials) {
    let appClient = new Client.IotfApplication(credentials);
    appClient.connect();
    appClient.on('connect', () => {
      console.log('[iot-worker] Successful connection to Watson IoT');
      appClient.subscribeToDeviceEvents('sigfox-auth-device'); // This is defined in the Sigfox callback & IoT
      appClient.subscribeToDeviceEvents('sigfox-auth-device-v2'); // This is defined in the Sigfox callback & IoT
    });
    appClient.on('deviceEvent', receivedDeviceEvent);
  }
}

function receivedDeviceEvent(deviceType, deviceId, eventType, format, payloadBuffer) {
  // Sigfox dev payload:
  // { time: '1503489571',
  //   deviceType: 'test-device',
  //   device: '1A900',
  //   duplicate: 'false',
  //   snr: '20.00',
  //   rssi: '-131.00',
  //   avgSnr: '20.00',
  //   station: '0001',
  //   lat: '44.0',
  //   lng: '2.0',
  //  seqNumber: '1879',
  //  data: '77f867c181111362' }
  console.log('[iot-worker:receivedDeviceEvent:%s] Received data deviceType=%s', deviceId, deviceType);
  let tdId, payload, device;

  async.series([
    // Parse IoT payload & create technical data if non-simulated data
    cb => {
      try {
        payload = JSON.parse(payloadBuffer.toString());
      } catch (e) {
        return cb(new Error('Unable to parse JSON (' + e.message + ')'));
      }
      if (!payload) {
        return cb(new Error('No payload found'));
      }
      if (!payload.simulated) {
        console.log('[iot-worker:receivedDeviceEvent:%s] Payload: %j', deviceId, payload);
        TechnicalData.create({receivedDate: new Date(), receivedData: payload}, (err, technicalData) => {
          if (err) {
            console.error('[iot-worker:receivedDeviceEvent:%s] Error while inserting Technical Data (%j)', deviceId, err);
          } else {
            tdId = technicalData.id;
            console.log('[iot-worker:receivedDeviceEvent:%s:%s] Successfully saved technical data', deviceId, tdId);
            payload.technicalDataId = technicalData.id;
          }
          return cb();
        });
      } else {
        return cb();
      }
    },
    // Get device if it exists
    cb => {
      if (!payload || !payload.device) {
        return cb(new Error('No payload or no payload.device found'));
      }
      Device.findById(payload.device, {'include': 'container'}, (err, _device) => {
        if (err) {
          return cb(new Error('Unable to query device (' + err.message + ')'));
        }
        if (!_device) {
          return cb(new Error('No device exists for id=' + payload.device));
        }
        device = _device;
        console.log('[iot-worker:receivedDeviceEvent:%s:%s] Successfully found device', deviceId, tdId);
        return cb();
      });
    },
    // Update type if needed
    cb => {
      const isV2 = (deviceType === 'sigfox-auth-device-v2') ? true : false;

      if (device.container().isV2 !== isV2) {
        device.container().updateAttribute('isV2', isV2, {validate: false}, err => {
          if (err) {
            console.warn('[iot-worker:receivedDeviceEvent:%s] Unable to save updated container', deviceId);
          }
          return cb();
        });
      } else {
        return cb();
      }
    }
  ], err => {
    if (err) {
      console.error('[iot-worker:receivedDeviceEvent:%s:%s] Error encountered: %s', deviceId, tdId, err.message);
      return;
    }
    enqueueIoTMessage(device, deviceType, payload);
  });
}

function parseAttributes(data, deviceType) {
  if (deviceType === 'sigfox-auth-device') {
    // v1 (Batt + 1 Wifi MAC)
    // 0,  2: Battery level - 1 byte
    // 2,  4: Temperature - 1 byte
    // 4,  6: Moving - 1 byte
    // 6, 18: Wifi MAC - 6 bytes
    const newPayload = {};
    if (data && data.length >= 18) {
      newPayload.batteryLevel = parseBatteryLevel(data.substring(0, 2));
      let temperatureStr = data.substring(2, 4);
      newPayload.temperature = parseInt(temperatureStr, 16);
      let movingStr = data.substring(4, 6);
      if (parseInt(movingStr, 16) === 0) {
        newPayload.moving = false;
      } else {
        newPayload.moving = true;
      }
      const wifiMac = parseWifi(data.substring(6, 18));
      if (wifiMac !== '00:00:00:00:00:00') {
        newPayload.wifiMac = wifiMac;
      }
    }
    return newPayload;
  } else if (deviceType === 'sigfox-auth-device-v2') {
    // v2 (Wifi x2 or battery)
    // - 4 bytes: battery only
    // - 6 or 12 bytes: Wifi x1 or x2
    const newPayload = {};
    if (data && data.length === 4) {
      // Battery
      newPayload.batteryLevel = parseBatteryLevel(data.substring(0, 2));
    } else if (data && data.length === 12) {
      // Wifi MAC x1
      const wifiMac = parseWifi(data.substring(0, 12));
      if (wifiMac !== '00:00:00:00:00:00') {
        newPayload.wifiMac = wifiMac;
      }
    } else if (data && data.length === 24) {
      // Wifi MAC x2
      const wifiMac1 = parseWifi(data.substring(0, 12));
      if (wifiMac1 !== '00:00:00:00:00:00') {
        newPayload.wifiMac = wifiMac1;
      }
      const wifiMac2 = parseWifi(data.substring(12, 24));
      if (wifiMac2 !== '00:00:00:00:00:00') {
        newPayload.wifiMacAdd = wifiMac2;
      }
    } else {
      console.warn('[iot-worker:parseAttributes] Invalid payload data length for %s, discarding', data);
    }
    return newPayload;
  } else {
    console.warn('[iot-worker:parseAttributes] Unexpected device type (%s)', deviceType);
    return null;
  }
}

function parseBatteryLevel(val) {
  let batteryLevel = parseInt(val, 16);
  return ((batteryLevel / 255) * 3.4) * 2;
}

function parseWifi(val) {
  return val.substring(0, 2) + ':' + val.substring(2, 4) + ':' +
    val.substring(4, 6) + ':' + val.substring(6, 8) + ':' +
    val.substring(8, 10) + ':' + val.substring(10, 12);
}

function parseGeoPosition(payload, deviceType) {
  if (deviceType === 'sigfox-auth-device') {
    return {
      geopoint: new loopback.GeoPoint({'lat': parseFloat(payload.lat), 'lng': parseFloat(payload.lng)}),
      radiusInMeters: parseFloat(payload.radius),
      source: 'Network'
    };
  } else if (deviceType === 'sigfox-auth-device-v2') {
    return {
      geopoint: new loopback.GeoPoint({lat: payload.lat, lng: payload.lng}),
      radiusInMeters: payload.radius,
      source: payload.source
    };
  } else {
    console.warn('[iot-worker:parseGeoPosition] Unexpected device type (%s)', deviceType);
  }
}

function enqueueIoTMessage(device, deviceType, payload) {
  if (payload.seqNumber === null || payload.seqNumber === 'undefined') {
    console.warn('[iot-worker:enqueueIoTMessage:%s] No seqNumber, discarding', device.id);
    return;
  }

  const processId = device.id + ':' + payload.seqNumber;

  if (!msgQ[processId]) {
    msgQ[processId] = {
      device: device,
      deviceType: deviceType,
      deviceDate: new Date(parseInt(payload.time) * 1000),
      firstReveivedDate: new Date()
    };
  }

  if (payload.data) {
    console.log('[iot-worker:enqueueIoTMessage:%s] Queuing non geo position message (attributes)', device.id);
    const attributes = parseAttributes(payload.data, deviceType);
    msgQ[processId].attributes = attributes;
  } else if (payload.radius) {
    console.log('[iot-worker:enqueueIoTMessage:%s] Queuing geo position message', device.id);
    const geoPosition = parseGeoPosition(payload, deviceType);
    msgQ[processId].geoPosition = geoPosition;
  } else {
    console.warn('[iot-worker:enqueueIoTMessage:%s] Discarding useless message (no attributes, no geo position)', device.id);
    return;
  }
  checkQueue(processId);
}

function checkQueue(processId) {
  if (msgQ[processId].attributes && msgQ[processId].geoPosition) {
    if (msgQ[processId].deviceType === 'sigfox-auth-device' && msgQ[processId].attributes.wifiMac) {
      Site.findByWifiMac([msgQ[processId].attributes.wifiMac], (err, site) => {
        if (err) {
          console.log('[iot-worker:enqueueMessage:%s] Failed to resolve MAC@ resolution for sites (%j)', msgQ[processId].device.id, err);
        }
        if (site) {
          console.log('[iot-worker:enqueueMessage:%s] Successfully resolved MAC@: %s to PSA site %s',
            msgQ[processId].device.id,
            msgQ[processId].attributes.wifiMac,
            site.name);
          msgQ[processId].wifiMacSitePos = site;
          send(processId);
        } else {
          wifiLocator.singleWifiGeoLoc(msgQ[processId].attributes.wifiMac, (err, res) => {
            if (!err && res) {
              console.log('[iot-worker:enqueueMessage:%s] Resolved Wifi position', msgQ[processId].device.id);
              msgQ[processId].wifiGeoPos = res;
            } else {
              console.log('[iot-worker:enqueueMessage:%s] Failed to resolve Wifi position', msgQ[processId].device.id);
            }
            send(processId);
          });
        }
      });
    } else if (msgQ[processId].deviceType === 'sigfox-auth-device-v2' &&
               msgQ[processId].attributes.wifiMac &&
               msgQ[processId].geoPosition.source === 'Network') {
      console.log('[iot-worker:enqueueMessage:%s] Will attempt to resolve a v2 network position', msgQ[processId].device.id);
      Site.findByWifiMac([msgQ[processId].attributes.wifiMac, msgQ[processId].attributes.wifiMacAdd], (err, site) => {
        if (err) {
          console.log('[iot-worker:enqueueMessage:%s] Failed to resolve MAC@ resolution for sites (%j)', msgQ[processId].device.id, err);
        }
        if (site) {
          console.log('[iot-worker:enqueueMessage:%s] Successfully resolved MAC@: %s to PSA site %s',
            msgQ[processId].device.id,
            msgQ[processId].attributes.wifiMac,
            site.name);
          msgQ[processId].wifiMacSitePos = site;
          send(processId);
        } else {
          wifiLocator.singleWifiGeoLoc([msgQ[processId].attributes.wifiMac, msgQ[processId].attributes.wifiMacAdd], (err, res) => {
            if (!err && res) {
              console.log('[iot-worker:enqueueMessage:%s] Resolved Wifi position', msgQ[processId].device.id);
              msgQ[processId].wifiGeoPos = res;
              app.models.WifilocAudit.create({
                deviceId: msgQ[processId].device.id,
                date: msgQ[processId].deviceDate,
                wifiLoc: msgQ[processId].wifiGeoPos,
                details: 'Wifi position resolved by mylnikov.org over missing HERE data for v2 device'
              });
            } else {
              console.log('[iot-worker:enqueueMessage:%s] Failed to resolve Wifi position', msgQ[processId].device.id);
            }
            send(processId);
          });
        }
      });
    } else {
      send(processId);
    }
  }
}

function send(processId) {
  console.log('[iot-worker:send:%s] About to record device event', msgQ[processId].device.id);
  console.log(' - Device date  : %s', msgQ[processId].deviceDate);
  console.log(' - Device type  : %s', msgQ[processId].deviceType);
  console.log(' - Recvd date   : %s', msgQ[processId].firstReveivedDate);
  console.log(' - Attrs        : %s', !!msgQ[processId].attributes);
  console.log(' - GeoPos       : %s', !!msgQ[processId].geoPosition);
  console.log(' - GeoPos source: %s', msgQ[processId].geoPosition.source);
  console.log(' - WiFiGeoPos   : %s', !!msgQ[processId].wifiGeoPos);
  console.log(' - WiFiMacPos   : %s', !!msgQ[processId].wifiMacSitePos);

  let attributes = msgQ[processId].attributes;
  if (attributes) {
    attributes.timestamp = msgQ[processId].deviceDate;
  }

  let geoPosition = msgQ[processId].geoPosition;
  if (geoPosition) {
    geoPosition.timestamp = msgQ[processId].deviceDate;
  }

  // Check if wifi geo position is consistent when wifi pos is provided
  if (msgQ[processId].wifiGeoPos && geoPosition) {
    // There is a wifi geoposition resolved
    const distance = msgQ[processId].wifiGeoPos.geopoint.distanceTo(geoPosition.geopoint, {type: 'meters'});
    console.log('[iot-worker:send:%s] Distance (wifi, sigfox): %d kms', msgQ[processId].device.id, distance / 1000);
    if (distance < 1000 * app._internalConfig.MYLNIKOV_TOLERANCE_KMS) {
      // Wifi geoposition is valid
      geoPosition.geopoint = msgQ[processId].wifiGeoPos.geopoint;
      geoPosition.radiusInMeters = msgQ[processId].wifiGeoPos.radiusInMeters;
      geoPosition.source = 'Wifi (mylnikov.org)';
      console.log(' - ResolvedPos  : Wifi');
    } else {
      // Wifi geoposition is not valid
      geoPosition.source = 'Network (wifi mylnikov.org was invalid or less precise)';
      app.models.WifilocAudit.create({
        deviceId: msgQ[processId].device.id,
        date: msgQ[processId].deviceDate,
        wifiLoc: msgQ[processId].wifiGeoPos,
        sigfoxRadioLoc: geoPosition,
        details: 'Wifi resolved by mylnikov.org is too far from Sigfox network position'
      });
      console.log(' - ResolvedPos  : Network (wifi was too far)');
    }
  } else if (msgQ[processId].wifiMacSitePos) {
    geoPosition.geopoint = new loopback.GeoPoint(msgQ[processId].wifiMacSitePos.geopoint);
    geoPosition.radiusInMeters = 0;
    geoPosition.source = 'Wifi (Site resolution)';
    app.models.WifilocAudit.create({
      deviceId: msgQ[processId].device.id,
      date: msgQ[processId].deviceDate,
      wifiLoc: msgQ[processId].wifiMacSitePos,
      sigfoxRadioLoc: geoPosition,
      details: 'Wifi site matching'
    });
    console.log(' - ResolvedPos  : MAC@ Site matching');
  }
  if (geoPosition) {
    msgQ[processId].device.recordGeoPosition(msgQ[processId].device.container().containerTypeId, geoPosition, err => {
    });
  }
  if (attributes) {
    msgQ[processId].device.recordAttributes(attributes, err => {
    });
  }

  // Remove from queue, it's processed!
  delete msgQ[processId];
}

module.exports = {
  'start': start,
  'receivedDeviceEvent': receivedDeviceEvent
};
