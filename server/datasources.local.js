'use strict';

const EnvParser = require('bluemix-utils-node/env-parser');
let envParser = new EnvParser(__dirname + '/../env.json');
const mongoOnComposeDbName = envParser.get('MONGO_ON_COMPOSE_DB_NAME');

let dsAndDbName = 'psa-tnt';
if (mongoOnComposeDbName) {
  console.log('Using custom db name: %s', mongoOnComposeDbName);
  dsAndDbName += '/' + mongoOnComposeDbName;
} else {
  console.log('Using default db name: %s', dsAndDbName);
}

module.exports = require('bluemix-utils-node/loopback-mongo-ds')(dsAndDbName, __dirname);
