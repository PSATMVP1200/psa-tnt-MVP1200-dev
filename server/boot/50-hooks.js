'use strict';

const async = require('async');

let Device, Init;

module.exports = function(app, mainCb) {
  Device = app.models.Device;
  Init = app.models.Init;
  Init.findOne({}, (err, foundInit) => {
    if (err) {
      return mainCb(err);
    }
    const funcs = [];
    if (foundInit && !foundInit.lastUpdateOnDevicesMigrated) {
      console.log('[50-hooks] Migration of devices IS required');
      funcs.push(migrateDevices);
    } else {
      console.log('[50-hooks] Migration of devices is not required');
    }
    funcs.push(recordHooks);

    async.series(funcs, mainCb);
  });
};

function migrateDevices(cb) {
  console.log('[50-hooks.migrateDevices]');

  Device.find({}, (err, devices) => {
    if (err) {
      return cb(err);
    }
    async.eachLimit(devices, 50, (device, cb) => {
      if (device.lastUpdate) {
        console.log('[50-hooks.migrateDevices] Skipping device %s (already migrated)', device.id);
        return cb();
      }
      let lastUpdate;
      if (device.currentGeoPosition && device.currentAttribute) {
        lastUpdate = device.currentGeoPosition.timestamp > device.currentAttribute.timestamp ? device.currentGeoPosition.timestamp : device.currentAttribute.timestamp;
      } else if (device.currentGeoPosition) {
        lastUpdate = device.currentGeoPosition.timestamp;
      } else if (device.currentAttribute) {
        lastUpdate = device.currentAttribute.timestamp;
      } else {
        console.warn('[50-hooks.migrateDevices] Setting default date (1/1/1970) for %s (no valid date found)', device.id);
        lastUpdate = new Date(0);
      }
      console.log('[50-hooks.migrateDevices] Migrating device %s', device.id);
      device.updateAttribute('lastUpdate', lastUpdate, {'validate': false}, cb);
    }, err => {
      if (err) {
        return cb(err);
      }
      Init.findOne({}, (err, foundInit) => {
        if (err) {
          return cb(err);
        }
        foundInit.lastUpdateOnDevicesMigrated = true;
        foundInit.save(cb);
      });
    });
  });
}

function recordHooks(cb) {
  Device.observe('before save', (ctx, next) => {
    if (ctx.data) {
      if (ctx.data.currentAttribute) {
        console.log('[50-hooks.recordHooks] Updating device %s lastUpdate (attribute)', ctx.currentInstance.id);
        ctx.data.lastUpdate = ctx.data.currentAttribute.timestamp;
      } else if (ctx.data.currentGeoPosition) {
        console.log('[50-hooks.recordHooks] Updating device %s lastUpdate (geopos)', ctx.currentInstance.id);
        ctx.data.lastUpdate = ctx.data.currentGeoPosition.timestamp;
      }
    }
    next();
  });
  return cb();
}
