/* eslint-disable max-len,space-before-function-paren */
'use strict';

module.exports = function (app) {
  app.models.Site.find({}, (err, allSites) => {
    if (err) {
      throw new Error('[cache] Unable to retrieve all sites (for caching)');
    } else {
      console.log('[cache] All sites are now in cache (total: %d)', allSites.length);
      app._cacheData.allSites = allSites;
    }
  });
};
