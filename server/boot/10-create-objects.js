'use strict';

const _ = require('underscore'),
  async = require('async'),
  loopback = require('loopback');

let Login, Device, Site, ContainerType, Init;

module.exports = function(app, mainCb) {
  Login = app.models.Login;
  Device = app.models.Device;
  Site = app.models.Site;
  ContainerType = app.models.ContainerType;
  Init = app.models.Init;

  Init.findOne({}, (err, foundInit) => {
    if (foundInit && foundInit.baseObjectsCreated) {
      console.log('Create objects: objects already created, skipping');
      return mainCb();
    } else {
      createObjects((err) => {
        if (err) {
          return mainCb(err);
        }
        if (foundInit) {
          foundInit.baseObjectsCreated = true;
          foundInit.save(mainCb);
        } else {
          Init.create({'baseObjectsCreated': true}, mainCb);
        }
      });
    }
  });
}

function createObjects(cb) {
  let logins, containerTypes, devicesAndContainers, sites;
  try {
    logins = require('../../data/logins.json');
  } catch (e) {
  }
  try {
    devicesAndContainers = require('../../data/devices-and-containers.json');
  } catch (e) {
  }
  try {
    containerTypes = require('../../data/container-types.json');
  } catch (e) {
  }
  try {
    sites = require('../../data/sites.json');
  } catch (e) {
  }
  async.series([
    // Users
    (cb) => {
      if (!logins) {
        console.log('boot: no logins need to be provisoned');
        return cb();
      }
      async.mapSeries(logins, (login, cb) => {
        Login.findOne({'where': {'username': login.username}}, (err, found) => {
          if (err) {
            return cb(err);
          }
          if (!found) {
            console.log('boot: Creating user %s', login.username);
            return Login.create(login, cb);
          } else {
            console.log('boot: Skipping existing user %s', login.username);
            return cb();
          }
        });
      }, cb);
    },
    // Sites
    (cb) => {
      if (!sites) {
        console.log('boot: no sites need to be provisoned');
        return cb();
      }
      async.map(sites, (site, cb) => {
        Site.findOne({'where': {'name': site.name}}, (err, found) => {
          if (err) {
            return cb(err);
          }
          if (!found) {
            site.geopoint = loopback.GeoPoint({
              'lat': site.lat,
              'lng': site.lng
            });
            delete site.lat;
            delete site.lng;
            let wifiAccessPoints = site.wifiAccessPoints;
            delete site.wifiAccessPoints;
            Site.create(site, (err, _site) => {
              if (err) {
                return cb(err);
              }

              if (wifiAccessPoints) {
                async.map(
                  wifiAccessPoints,
                  (wifi, cb) => {
                    _site.wifiAccessPoints.create({
                      macAddress: wifi.macAddress.toLowerCase(),
                      geopoint: loopback.GeoPoint({
                        'lat': _site.geopoint.lat,
                        'lng': _site.geopoint.lng
                      })
                    }, cb);
                  }, cb);
              } else {
                return cb();
              }
            });
          } else {
            console.log('boot: Skipping creation of existing site with name=%s', site.name);
            let wifiAccessPoints = site.wifiAccessPoints;
            found.wifiAccessPoints.destroyAll((err) => {
              if (wifiAccessPoints) {
                async.map(wifiAccessPoints, (wifi, cb) => {
                  let tocreate = {
                    macAddress: wifi.macAddress.toLowerCase(),
                    geopoint: loopback.GeoPoint({'lat': site.lat, 'lng': site.lng})
                  };
                  found.wifiAccessPoints.create(tocreate, cb);
                }, cb);
              } else {
                return cb();
              }
            });
          }
        });
      }, cb);
    },
    // Container types
    (cb) => {
      if (!containerTypes) {
        console.log('boot: no container-types need to be provisoned');
        return cb();
      }
      async.mapValues(containerTypes, (containerType, containerTypeName, cb) => {
        ContainerType.findOne({'where': {'type': containerTypeName}}, (err, found) => {
          if (err) {
            return cb(err);
          }
          if (!found) {
            containerType.type = containerTypeName;
            console.log('boot: Creating container-type with type=%s', containerTypeName);
            return ContainerType.create(containerType, cb);
          } else {
            console.log('boot: Skipping creation of existing container-type with type=%s', containerTypeName);
            return cb();
          }
        });
      }, cb);
    },
    // Devices
    (cb) => {
      if (!devicesAndContainers) {
        console.log('boot: no devices-and-containers need to be provisoned');
        return cb();
      }
      async.mapValues(devicesAndContainers, (devices, containerTypeName, cb) => {
        ContainerType.findOne({'where': {'type': containerTypeName}},
          (err, foundContainerType) => {
            if (err) {
              return cb(err);
            }
            if (!foundContainerType) {
              return cb(new Error('No container-type found for type=' + containerTypeName));
            }
            console.log('[Re-]creating devices for container-type with type=%s', containerTypeName);
            let simulatedDevices = _.where(devices, {simulate: true});
            let simulatedDevicesId = _.pluck(simulatedDevices, 'id');
            Device.destroyAll({id: {inq: simulatedDevicesId}}, (err) => {
              if (err) {
                console.log('Error destroying device', err);
              }
              // Create all devices & containers
              async.map(devices, (device, cb) => {
                Device.create(device, (err, createdDevice) => {
                  if (err) {
                    return cb(err);
                  }
                  createdDevice.container.create({'containerTypeId': foundContainerType.id}, cb);
                });
              }, cb);
              console.log('Should create device(s) and container(s)');
              console.log('- ContainerType: %j', foundContainerType);
              console.log('- Devices: %j', devices);
            });
          });
      }, cb);
    }
  ], cb);
};
