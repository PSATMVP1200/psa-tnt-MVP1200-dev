'use strict';

const _ = require('underscore'),
  async = require('async'),
  errReduce = require('../../common/lib/err-reduce');

let Device, Container, DeviceState, lowBatteryThreshold, maxQuietTimeMs;

module.exports = function(app) {
  console.log('scheduled-tasks Will run every %d minute(s)', app._internalConfig.SCHEDULER_PERIOD_MINS);
  lowBatteryThreshold = app._internalConfig.LOW_BATTERY_THRESHOLD;
  maxQuietTimeMs = app._internalConfig.MAX_QUIET_MINS * 60 * 1000;
  Device = app.models.Device;
  Container = app.models.Container;
  DeviceState = app.models.DeviceState;

  const scheduler = setInterval(() => {
    console.log('scheduled-tasks Running scheduled tasks');
    async.series([
      (cb) => {
        unmarkContainersWithNormalBattery((err) => {
          if (err) {
            console.log('scheduled-tasks Error occurred while attempting to unmark containers with low battery (%s)', err.message);
          }
          return cb();
        });
      },
      (cb) => {
        markContainersWithLowBattery((err) => {
          if (err) {
            console.log('scheduled-tasks Error occurred while attempting to mark containers with low battery (%s)', err.message);
          }
          return cb();
        });
      },
      (cb) => {
        unmarkNonQuietContainers((err) => {
          if (err) {
            console.log('scheduled-tasks Error occurred while attempting to unmark non-quiet containers (%s)', err.message);
          }
          return cb();
        });
      },
      (cb) => {
        markQuietContainers((err) => {
          if (err) {
            console.log('scheduled-tasks Error occurred while attempting to mark quiet containers (%s)', err.message);
          }
          return cb();
        });
      }]);
  }, app._internalConfig.SCHEDULER_PERIOD_MINS * 60 * 1000);
};

function markContainersWithLowBattery(cb) {
  Device.find({
    'where': {
      'currentAttribute.batteryLevel': { // This works because currentDeviceState is embedded within device
        'lt': lowBatteryThreshold
      }
    }
  }, (err, foundDevices) => {
    if (err) {
      console.error('markContainersWithLowBattery Unable to query device for battery level: %s', errReduce(err));
      return cb(err);
    }
    if (!foundDevices || !Array.isArray(foundDevices) || foundDevices.length === 0) {
      console.log('markContainersWithLowBattery No devices are in low battery');
      return cb();
    }
    async.map(foundDevices, (foundDevice, cb) => {
      return foundDevice.container((err, container) => {
        if (err) {
          console.error('markContainersWithLowBattery Unable to query container for device %s (%s)',
            foundDevice.id,
            errReduce(err));
          return cb();
        }
        if (!container.isLowBattery) {
          container.updateAttribute('isLowBattery', true, {'validate': false}, (err) => {
            if (err) {
              console.error('markContainersWithLowBattery Unable to save container for device %s (%s)',
                container.id,
                errReduce(err));
            }
            console.log('markContainersWithLowBattery Container %s is now low battery', container.id);
            return cb(null);
          });
        } else {
          return cb();
        }
      });
    }, cb);
  });
}

function unmarkContainersWithNormalBattery(cb) {
  Container.find({
    'where': {
      'isLowBattery': true
    }
  }, (err, foundContainers) => {
    if (err) {
      console.error('unmarkContainersWithNormalBattery Unable to query containers: %s', errReduce(err));
      return cb(err);
    }
    if (!foundContainers || !Array.isArray(foundContainers) || foundContainers.length === 0) {
      console.error('unmarkContainersWithNormalBattery No containers currently flagged with low battery');
      return cb();
    }
    async.map(foundContainers, (foundContainer, cb) => {
      foundContainer.device((err, device) => {
        if (err) {
          console.error('unmarkContainersWithNormalBattery Unable to find device for container %s (%s)',
            foundContainer.id,
            errReduce(err));
          return cb();
        }
        if (device.currentAttribute && device.currentAttribute.batteryLevel >= lowBatteryThreshold) {
          foundContainer.updateAttribute('isLowBattery', false, {'validate': false}, (err) => {
            if (err) {
              console.error('unmarkContainersWithNormalBattery Unable to save container for device %s (%s)',
                foundContainer.id,
                errReduce(err));
            }
            console.log('unmarkContainersWithNormalBattery Container %s is no longer low battery', foundContainer.id);
            return cb(null);
          });
        } else {
          return cb();
        }
      });
    }, cb);
  });
}

function markQuietContainers(cb) {
  const quietDate = new Date((new Date()).getTime() - maxQuietTimeMs);

  Device.find({
    'where': {
      'and': [{
        'lastUpdate': {
          'lt': quietDate
        }
      }, {
        'simulate': false // Don't mark simulated devices quiet
      }]
    },
    'include': 'container'
  }, (err, foundDevices) => {
    if (err) {
      console.error('markQuietContainers Unable to query device: %s', errReduce(err));
      return cb(err);
    }
    if (!foundDevices || !Array.isArray(foundDevices) || foundDevices.length === 0) {
      console.log('markQuietContainers No devices should be quiet');
      return cb();
    } else {
      console.log('markQuietContainers %d device(s) might be quiet', foundDevices.length);
    }
    async.each(foundDevices, (foundDevice, cb) => {
      if (foundDevice.container().isQuiet) {
        return cb();
      }
      console.log('markQuietContainers Container %s should be quiet', foundDevice.container().deviceId);
      foundDevice.container().updateAttribute('isQuiet', true, {'validate': false}, (err) => {
        if (err) {
          console.error('markQuietContainers Unable to save container for device %s (%s)',
            foundDevice.container().deviceId,
            errReduce(err));
        }
        markCurrentGeoPosition(foundDevice.container(), 'quietTransition', 'to quiet', cb);
      });
    }, cb);
  });
}

function unmarkNonQuietContainers(cb) {
  Container.find({
    'where': {
      'isQuiet': true
    },
    'include': 'device'
  }, (err, foundContainers) => {
    if (err) {
      console.error('unmarkNonQuietContainers Unable to query containers: %s', errReduce(err));
      return cb(err);
    }
    if (!foundContainers || !Array.isArray(foundContainers) || foundContainers.length === 0) {
      console.error('unmarkNonQuietContainers No containers currently flagged as quiet');
      return cb();
    } else {
      console.log('markQuietContainers There are %d quiet container(s)', foundContainers.length);
    }
    async.each(foundContainers, (foundContainer, cb) => {
      if (foundContainer.device().simulate) { // Don't bother for simulated devices
        return cb();
      }

      if (!foundContainer.device().lastUpdate) {
        console.error('unmarkNonQuietContainers Device %s has no lastUpdate, it must be migrated (%s)', foundContainer.deviceId);
        return cb();
      }

      if (Date.now() - foundContainer.device().lastUpdate.getTime() >= maxQuietTimeMs) {
        return cb();
      }
      foundContainer.updateAttribute('isQuiet', false, {'validate': false}, (err) => {
        if (err) {
          console.error('unmarkNonQuietContainers Unable to save container for device %s (%s)',
            foundContainer.deviceId,
            errReduce(err));
        }
        console.log('unmarkNonQuietContainers Container %s is no longer quiet', foundContainer.deviceId);
        markCurrentGeoPosition(foundContainer, 'quietTransition', 'to normal', cb);
      });
    }, cb);
  });
}

function markCurrentGeoPosition(container, key, value, cb) {
  container.device((err, device) => {
    if (err) {
      return cb(err);
    }
    device.geoPositions.findOne({'order': 'timestamp DESC'}, (err, geoPosition) => {
      if (err) {
        return cb(err);
      }
      if (!geoPosition) {
        return cb();
      }
      geoPosition.updateAttribute(key, value, err => {
        if (err) {
          return cb(err);
        }
        device.updateAttribute('currentGeoPosition.quietTransition', value, {'validate': false}, cb);
      });
    });
  });
}
