'use strict';

const _ = require('underscore'),
  async = require('async'),
  moment = require('moment'),
  loopback = require('loopback');

const BASE_PRECISION_RADIUS_METERS = 2000, EXTRA_PRECISION_RADIUS_METERS = 4000,
  LAT_PRECISION = 0.02, LNG_PRECISION = 0.02;

let DeviceEvent, Container, Device, ContainerType, Site, Init, Status, GeoPosition;

module.exports = function(app, mainCb) {
  if (app._internalConfig.STATIC_DATA === 'false') {
    console.log('Static data: STATIC_DATA config variable is false, skipping');
    return mainCb();
  }
  DeviceEvent = app.models.DeviceEvent;
  Container = app.models.Container;
  Device = app.models.Device;
  ContainerType = app.models.ContainerType;
  Site = app.models.Site;
  Init = app.models.Init;
  Status = app.models.Status;
  GeoPosition = app.models.GeoPosition;

  let staticData;
  try {
    staticData = require('../../data/demo-static.json');
  } catch (e) {
  }
  if (!staticData) {
    console.log('Static data: no static data found, skipping');
    return mainCb();
  }

  // Check if static data was already initialized
  Init.findOne({}, (err, foundInit) => {
    if (foundInit && foundInit.staticDataCreated) {
      console.log('Static data: data already created, skipping');
      return mainCb();
    } else {
      return processStaticData(staticData, (err) => {
        if (err) {
          return mainCb(err);
        }
        if (foundInit) {
          foundInit.staticDataCreated = true;
          foundInit.save(mainCb);
        } else {
          Init.create({'staticDataCreated': true}, mainCb);
        }
      });
    }
  });
};

function processStaticData(staticData, mainCb) {
  console.log('Static data: About to start populating static data');

  async.map(staticData, (staticItem, cb) => {
    let devices, containers, containerType;
    async.waterfall([
      (cb) => {
        // 1. Find the right container type
        ContainerType.findOne({'where': {'type': staticItem.containerType}}, cb);
      },
      (_containerType, cb) => {
        // 2. Create devices for this container type
        containerType = _containerType;
        const devices = _.map(staticItem.devices, (deviceId) => {
          return {'id': deviceId, 'type': staticItem.deviceType, 'simulate': true};
        });
        Device.create(devices, cb);
      },
      (_devices, cb) => {
        // 3. Create a container for each device
        devices = _devices;
        if (!devices || !Array.isArray(devices) || devices.length === 0) {
          return cb(new Error('Static data: no devices were created, aborting'));
        }
        async.map(devices, (device, cb) => {
          device.container.create({
            'containerTypeId': containerType.type,
            'isQuiet': !!staticItem.isQuiet
          }, cb);
        }, cb);
      },
      (_containers, cb) => {
        // 4. Set container & device data
        containers = _containers;
        if (!containers || !Array.isArray(containers) || containers.length === 0) {
          return cb(new Error('Static data: no containers were created, aborting'));
        }
        async.eachOf(containers, (container, index, cb) => {
          setDeviceAndContainerData(devices[index], container, staticItem.data, cb);
        }, cb);
      }
    ], cb);
  }, mainCb);
}

function setDeviceAndContainerData(device, container, data, cb) {
  if (!data || !Array.isArray(data) || data.length === 0) {
    return cb(null, null);
  }
  const attributes = itemsToAttributes(data),
    geoPositions = itemsToGeoPositions(data);

  let currentSite;
  const orderedSites = _.pluck(_.filter(geoPositions, (geoPosition) => {
    if (!geoPosition.siteId) {
      return false;
    }
    if (geoPosition.siteId !== currentSite) {
      currentSite = geoPosition.siteId;
      return true;
    }
    return false;
  }), 'siteId');
  if (!_.last(geoPositions).siteId) {
    orderedSites.push(null);
  }

  async.map(geoPositions, (item, cb) => {
    device.geoPositions.create(item, (err, savedGeoPosition) => {
      if (err) {
        return cb(err);
      }
      device.attributes.create(attributes, err => {
        if (err) {
          return cb(err);
        }
        return cb(null, savedGeoPosition);
      });
    });
  }, (err, savedGeoPositions) => {
    if (err) {
      return cb(err);
    }
    const lastSavedGeoPosition = _.last(savedGeoPositions), lastAttribute = _.last(attributes);
    async.parallel([
      cb => {
        container.updateAttribute('currentStatus', lastSavedGeoPosition.status, cb);
      },
      cb => {
        let deviceAttributes = {
          'currentGeoPosition': lastSavedGeoPosition,
          'currentAttribute': lastAttribute
        };
        const cSiteId = orderedSites.pop(),
          lSiteId = orderedSites.pop(),
          bSiteId = orderedSites.pop();
        if (cSiteId) {
          deviceAttributes.currentSiteInTrafficId = cSiteId;
        }
        if (lSiteId) {
          deviceAttributes.lastSiteInTrafficId = lSiteId;
        }
        if (bSiteId) {
          deviceAttributes.beforeLastSiteInTrafficId = bSiteId;
        }
        device.updateAttributes(deviceAttributes, cb);
      }
    ], cb);
  });
}

function itemsToGeoPositions(items) {
  return _.map(items, (item) => {
    let lat = parseFloat(item.lat), lng = parseFloat(item.lng);
    if (isNaN(lat) || isNaN(lng) || !item.status) {
      return null;
    }

    const date = toDate(item.date);
    if (!item.site && !item.noRandom) { // No random for sites
      lat += 2 * LAT_PRECISION * Math.random() - LAT_PRECISION;
      lng += 2 * LNG_PRECISION * Math.random() - LNG_PRECISION;
    }
    let geoPosition = {
      'timestamp': date,
      'source': 'device',
      'radiusInMeters': BASE_PRECISION_RADIUS_METERS + Math.floor(Math.random() * EXTRA_PRECISION_RADIUS_METERS),
      'geopoint': loopback.GeoPoint({
        'lat': lat,
        'lng': lng
      }),
      'status': {
        'description': item.status.description,
        'marker': item.status.marker
      }
    };
    if (item.site) {
      geoPosition.siteId = item.site;
    }
    if (item.quietTransition) {
      geoPosition.quietTransition = item.quietTransition;
    }
    return geoPosition;
  });
}

function itemsToAttributes(items) {
  return _.compact(_.map(items, item => { // _.compact will remove null values from array
    if (item.batteryLevel || item.temperature) {
      let attribute = {};
      attribute.timestamp = toDate(item.date);
      if (item.batteryLevel) {
        attribute.batteryLevel = item.batteryLevel;
      }
      if (item.temperature) {
        attribute.temperature = item.temperature;
      }
      return attribute;
    } else {
      return null;
    }
  }));
}

function toDate(dateString) {
  const m = moment(dateString, 'DD/MM/YYYY HH:mm');
  if (!m.isValid()) {
    throw new Error('Static data: invalid date ' + dateString + ', aborting');
  }
  return m.toDate();
}
