/* eslint-disable max-len */
'use strict';

const iotWorker = require('../../common/lib/iot-worker'),
  async = require('async');

module.exports = function(app) {

  console.warn('** /!\\ WARNING - SIMULATE DEVICE IS CURRENTLY DEACTIVATED**');

  /*setTimeout(() => {
    iotWorker.receivedDeviceEvent('deviceType', 'device', '', '', new Buffer(JSON.stringify({
      time: '1506358266',
      deviceType: 'test-device',
      device: '36E827',
      duplicate: 'false',
      snr: '25.42',
      rssi: '-126.00',
      avgSnr: '60.86',
      station: '3D6E',
      lat: '48.0',
      lng: '7.0',
      seqNumber: '1781',
      data: '70000064510617f3f2000000'
    })));
  }, 5000);*/
  return;

  console.log('simulate-device Will simulate a device event');
  const initPointsDurationInMs = 5000;
  // const durationBetweenPointsMINInSec = 85;
  // const durationBetweenPointsMAXInSec = 90;
  // const durationBetweenTrucksMINInSec = 120;
  // const durationBetweenTrucksMAXInSec = 220;
  // const durationBetweenPointsMINInSec = 210;
  // const durationBetweenPointsMAXInSec = 220;
  const durationBetweenTrucksMINInSec = 200;
  const durationBetweenTrucksMAXInSec = 300;
  let allTrucks = [];
  let truckStartDelayInMs = [];
  // let truckDurationBetweenPointsInMs = [];
  let devicesIdsByTruck = [];

  if (app._internalConfig.SIMULATE_DEVICES == 'true') {
    app.models.SimulatedTruck.find((err, trucks) => {
      if (err)
        console.error(err);
      console.log('[simulate-device] Found %s trucks', trucks.length);

      let currentTruck = 0;
      // trucks.forEach((cTruck) => {
      //   allTrucks[currentTruck] = cTruck;
      //   devicesIdsByTruck[currentTruck] = generateDeviceIds(cTruck);
      //   truckDurationBetweenPointsInMs[currentTruck] = (Math.floor((10 - 4) * Math.random()) + 4) * 1000;
      //   currentTruck++;
      // });

      async.series([
        (cb) => {
          // Remove All previous devices and events by containerType
          async.map(trucks, (truck, cb) => {
            removeDevicesAndEventsByContainerType(app, truck.containerType, cb);
          }, cb);
        },
        (cb) => {
          // Create all devices
          currentTruck = 0;
          async.map(trucks, (truck, cb) => {
            allTrucks[currentTruck] = truck;
            devicesIdsByTruck[currentTruck] = generateDeviceIds(truck);
            // Build random time of refresh between points for each truck (between 85 to 90 sec)
            // truckDurationBetweenPointsInMs[currentTruck] = (Math.floor((durationBetweenPointsMAXInSec - durationBetweenPointsMINInSec) * Math.random()) + durationBetweenPointsMINInSec) * 1000;
            truckStartDelayInMs[currentTruck] = (Math.floor((durationBetweenTrucksMAXInSec - durationBetweenTrucksMINInSec) * Math.random()) + durationBetweenTrucksMINInSec) * 1000;
            createDevices(app, devicesIdsByTruck[currentTruck], truck.containerType, cb);
            currentTruck++;
          }, cb);
        },
        (cb) => {
          currentTruck = 0;
          // Initialize first position for All trucks
          async.map(trucks, (truck, cb) => {
            if (truck && truck.route && truck.route.length > 0) {
              let currentPosition = truck.route[0];
              let initFunctions = [];
              devicesIdsByTruck[currentTruck].forEach((deviceId) => {
                initFunctions.push((cb) => {
                  setTimeout(() => {
                    sendPosition('sigfox-auth-device', deviceId, currentPosition.lat, currentPosition.lng);
                    cb();
                  }, initPointsDurationInMs);
                });
              });
              async.parallel(initFunctions, cb);
            }
            currentTruck++;
          }, cb);
        },
        (cb) => {
          currentTruck = 0;
          console.log('@@@@@@@@@@@@@ START');
          async.map(trucks, (truck, cb) => {
            let iteration = 0;
            let deviceIds = devicesIdsByTruck[currentTruck];
            for (iteration = 1; iteration < truck.route.length; iteration++) {
              // truck.route.forEach((route) => {
              const route = truck.route[iteration];
              //   iteration++;
              console.log('############ REGISTER ROUTE %s : %s  ', route, (truck.durationBetweenPoints * 1000 * iteration));
              setTimeout(() => {
//              const interval = setInterval(() => {
                let currentPosition = route;
                deviceIds.forEach((deviceId) => {
                  console.log('$$$$$$$$$$$$$$$$ RUN ROUTE %s for Device %s', route, deviceId);
                  sendPosition('sigfox-auth-device', deviceId, currentPosition.lat, currentPosition.lng);
                });
                if (iteration >= truck.route.length) {
                  console.log('[simulate-device] Truck %s End of journey!');
                  // reinitTruck(truck, devicesIdsByTruck[currentTruck]);
                  // iteration = 0;
                  //              clearInterval(interval);
                }
              }, (truck.durationBetweenPoints * 1000 * iteration));
              //          }, truckStartDelayInMs[currentTruck]); // Delay start between trucks...
            }
            // })
            currentTruck++;
          });
          cb();
        }
      ]);
    });
  }
};

// const startupDelayInMs = 5000,
//   // startLat = 47.778267, startLng = 7.436249, // PSA Mulhouse
//   // endLat = 47.509509, endLng = 6.812390, // PSA Sochaux
//   endLat = 47.778267, endLng = 7.436249, // PSA Mulhouse
//   startLat = 47.509509, startLng = 6.812390, // PSA Sochaux
//   nbPoints = 20, durationBetweenPointsInMs = 10000;
//
// let iteration = 1;
// setTimeout(() => {
//   console.log('simulate-device Start of journey!');
//   sendPosition('sigfox-auth-device', '0A901', startLat, startLng);
//
//   const interval = setInterval(() => {
//     console.log('simulate-device Journey %d of %d', iteration + 1, nbPoints);
//
//     const lng = startLng + (iteration * (endLng - startLng) / (nbPoints - 1)),
//       lat = startLat + (iteration * (endLat - startLat) / (nbPoints - 1));
//     sendPosition('sigfox-auth-device', '0A901', lat, lng);
//
//     if (++iteration >= nbPoints) {
//       console.log('simulate-device End of journey!');
//       clearInterval(interval);
//     }
//   }, durationBetweenPointsInMs);
// }, startupDelayInMs); // Give some time for app to start...

function removeDevicesAndEventsByContainerType(app, containerType, mainCb) {
  const where = {
    where: {and: [{containerTypeId: containerType}, {simulate: true}]}
  };

  app.models.Device.find(where, (err, devices) => {
    if (err) {
      console.error(err);
      mainCb();
    } else {
      async.map(devices, (device, cb) => {
        app.models.Device.destroyById([device.id], cb);
      }, mainCb);
    }
  });
//   app.models.ContainerType.find(where, (err, containerTypes) => {
//     if (err) {
//       console.error(err);
//       cb();
//     } else {
//       async.map(containerTypes, (containerType, cb) => {
//         containerType.configuredDevices.destroyAll((err) => {
//           containerType.containers.destroyAll(cb);
// //        cb();
//         });
//       }, cb);
//     }
//   });
}

function generateDeviceIds(truck) {
  let ids = [];
  const prefix = '36' + truck.containerIdPrefix + truck.truckContainerPrefix;
  const nbDeviceToGenerate = Math.floor((truck.nbContainerMax - truck.nbContainerMin) * Math.random()) + truck.nbContainerMin;

  for (let i = 0; i < nbDeviceToGenerate; i < nbDeviceToGenerate) {
    ids.push(prefix + i);
    i++;
  }
  return ids;
}

function createDevices(app, deviceIds, containerType, cb) {
  async.series([(cb) => {
    app.models.ContainerType.findOne({'where': {'type': containerType}},
      (err, foundContainerType) => {
        if (err) {
          return cb(err);
        }
        if (!foundContainerType) {
          return cb(new Error('No container-type found for type=' + containerType));
        }
        async.map(deviceIds, (deviceId, cb) => {
          const device = {
            id: deviceId,
            type: 'sigfox-auth-device',
            simulate: true
          };
          foundContainerType.configuredDevices.create(device, (err, dev) => {
            if (err) {
              console.log('Devices already exist');
              cb();
            } else {
              console.log('Devices created', dev);
              dev.container.create({
                'containerTypeId': containerType
              }, (err) => {
                if (err) {
                  console.log('Cannot Create Container', err);
                } else {
                  console.log('Container created');
                }
                cb();
              });
            }
          });
        }, cb);
      });
  }], cb);
}

function sendPosition(deviceType, device, lat, lng) {
  // 76 00 00 885bdd36bb95 000000
  let batteryLevel = 50;
  let temperature = Math.ceil(Math.random() * 40);
  if (temperature < 16) temperature = temperature + 16;
  let data = batteryLevel.toString(16);
  data = data + temperature.toString(16);
  // moving to true
  data = data + '01';
  // MAC to 0
  data = data + '000000000000';
  // data = data + 'CCBBCCDDEEFG'
  // data = data + '000C421F65E9'
  // PAD with unused
  data = data + '000000';

  iotWorker.receivedDeviceEvent(deviceType, device, '', '', new Buffer(JSON.stringify({
    'lat': lat,
    'lng': lng,
//    'wifiMac': '00:0C:42:1F:65:E9',
    'wifiMac': '',
    'data': data,
    'radius': Math.random() * 3000,
    'time': Date.now(),
    'simulated': true
  })));

  // iotWorker.receivedDeviceEvent(deviceType, device, '', '', new Buffer(JSON.stringify({
  //   'lat': lat,
  //   'lng': lng,
  //   'wifiMac': '',
  //   'batteryLevel': 50,
  //   'temperature': Math.random() * 40,
  //   'moving': Math.random() >= 0.5,
  //   'radiusInMeters': Math.random() * 1000,
  //   'time': Date.now()
  // })));
}
