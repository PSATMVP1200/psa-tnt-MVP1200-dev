'use strict';

const
  basicAuthConnect = require('basic-auth-connect'),
  EnvParser = require('bluemix-utils-node/env-parser'),
  iotWorker = require('../common/lib/iot-worker'),
  loopback = require('loopback'),
  boot = require('loopback-boot');

let app = module.exports = loopback();
app._internalConfig = {}, app._cacheData = {};
let envParser = new EnvParser(__dirname + '/../env.json');

let explorerBasicAuth = envParser.get('EXPLORER_BASIC_AUTH');
if (explorerBasicAuth && explorerBasicAuth.username && explorerBasicAuth.password) {
  console.log('Explorer is protected with basic auth');
  app.use('/explorer',
    basicAuthConnect(explorerBasicAuth.username, explorerBasicAuth.password));
}

let iotCredentials;
try {
  iotCredentials = envParser.get('VCAP_SERVICES')['iotf-service'][0].credentials;
} catch (e) {
}

let odmCredentials = envParser.get('VCAP_SERVICES')['businessrules'][0].credentials;
if (!odmCredentials) {
  throw new Error('Missing Business Rules credentials');
}

app._internalConfig.SCHEDULER_PERIOD_MINS = parseFloat(envParser.get('SCHEDULER_PERIOD_MINS')) || 60;
console.log('server Using SCHEDULER_PERIOD_MINS=%d', app._internalConfig.SCHEDULER_PERIOD_MINS);
app._internalConfig.LOW_BATTERY_THRESHOLD = parseInt(envParser.get('LOW_BATTERY_THRESHOLD')) || 1;
console.log('server Using LOW_BATTERY_THRESHOLD=%d', app._internalConfig.LOW_BATTERY_THRESHOLD);
app._internalConfig.MAX_QUIET_MINS = parseInt(envParser.get('MAX_QUIET_MINS')) || 4320; // 3 days
console.log('server Using MAX_QUIET_MINS=%d', app._internalConfig.MAX_QUIET_MINS);
app._internalConfig.ODM_CREDENTIALS = odmCredentials; //
console.log('server Using ODM_CREDENTIALS=', app._internalConfig.ODM_CREDENTIALS);
app._internalConfig.MAX_ACCEPTABLE_RADIUS = parseInt(envParser.get('MAX_ACCEPTABLE_RADIUS')) || 10000;
console.log('server Using MAX_ACCEPTABLE_RADIUS=', app._internalConfig.MAX_ACCEPTABLE_RADIUS);
app._internalConfig.SIMULATE_DEVICES = envParser.get('SIMULATE_DEVICES') || 'false';
app._internalConfig.STATIC_DATA = envParser.get('STATIC_DATA') || 'false';
console.log('server Using STATIC_DATA=%s', app._internalConfig.STATIC_DATA);
app._internalConfig.MYLNIKOV_TOLERANCE_KMS = parseFloat(envParser.get('MYLNIKOV_TOLERANCE_KMS')) || 100;
console.log('server Using MYLNIKOV_TOLERANCE_KMS=%d', app._internalConfig.MYLNIKOV_TOLERANCE_KMS);

app.start = () => {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    let baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);

    if (iotCredentials) {
      iotWorker.start({
        'org': iotCredentials.org,
        'id': envParser.get('VCAP_APPLICATION').name,
        'domain': 'internetofthings.ibmcloud.com',
        'auth-key': iotCredentials.apiKey,
        'auth-token': iotCredentials.apiToken
      });
    } else {
      console.warn('Missing iot credentials, no iot connection will take place');
    }
    if (app.get('loopback-component-explorer')) {
      let explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) {
    throw err;
  }
  // start the server if `$ node server.js`
  if (require.main === module) {
    app.start();
  }
});
