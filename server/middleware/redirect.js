module.exports = function() {
  return function redirect(req, res, next) {
    if (req.headers['x-forwarded-proto'] == 'http') {
      console.log('middleware:redirect Redirecting HTTP');
      res.redirect(301, 'https://' + req.get('host') + req.originalUrl);
    } else {
      next();
    }
  };
};
