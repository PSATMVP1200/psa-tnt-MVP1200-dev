'use strict';

const _ = require('underscore'),
  app = require('../server/server'),
  iotWorker = require('../common/lib/iot-worker'),
  wifiLocator = require('../common/lib/wifi-locator'),
  async = require('async'),
  chai = require('chai'),
  expect = chai.expect,
  moment = require('moment');

chai.use(require('chai-string'));

let server;

const deviceIdv1 = '367C1F', deviceIdv2 = '36844F',
  timeoutCheck = 1800;

xdescribe('IOT Corrections', function() {
  this.timeout(10000);
  before(startServer);
  after(stopServer);

  describe('(1) V1 - no site correction', function() {
    beforeEach(done => {
      app.models.GeoPosition.destroyAll({'deviceId': deviceIdv1}, done);
    });
    it('with no wifi in the middle of nowhere', function(done) {
      receivedDeviceEventPos('v1', deviceIdv1, 45.284712, 1.059003, 9000, 0);
      receivedDeviceEventAttrs('v1', deviceIdv1, 0, null, 0);
      setTimeout(() => {
        app.models.Device.findById(deviceIdv1, (err, device) => {
          expect(err).to.be.null;
          expect(device).to.have.property('currentGeoPosition');
          expect(device.currentGeoPosition).to.have.property('source');
          expect(device.currentGeoPosition.source).to.equal('Network');
          done();
        });
      }, timeoutCheck);
    });

    it('with matching wifi in the middle of nowhere', function(done) {
      const networkLat = 43.415628, networkLng = 6.077538; // Brignoles (>90 <100kms)
      receivedDeviceEventPos('v1', deviceIdv1, networkLat, networkLng, 9000, 0);
      receivedDeviceEventAttrs('v1', deviceIdv1, 0, 'a4:5e:60:d3:cb:b3', 0);
      setTimeout(() => {
        app.models.Device.findById(deviceIdv1, (err, device) => {
          expect(err).to.be.null;
          expect(device).to.have.property('currentGeoPosition');
          expect(device.currentGeoPosition).to.have.property('source');
          expect(device.currentGeoPosition.source).to.equal('Wifi (mylnikov.org)');
          expect(device.currentGeoPosition.geopoint.lat).to.equal(43.73210474767); // Wifi Pos
          expect(device.currentGeoPosition.geopoint.lng).to.equal(7.16753567864); // Wifi Pos
          done();
        });
      }, timeoutCheck);
    });

    it('with no matching wifi in the middle of nowhere', function(done) {
      const networkLat = 43.834872, networkLng = 5.824149; // Brignoles (>90 <100kms)
      receivedDeviceEventPos('v1', deviceIdv1, networkLat, networkLng, 9000, 0);
      receivedDeviceEventAttrs('v1', deviceIdv1, 0, 'a4:5e:60:d3:cb:b3', 0);
      setTimeout(() => {
        app.models.Device.findById(deviceIdv1, (err, device) => {
          expect(err).to.be.null;
          expect(device).to.have.property('currentGeoPosition');
          expect(device.currentGeoPosition).to.have.property('source');
          expect(device.currentGeoPosition.source).to.equal('Network (wifi mylnikov.org was invalid or less precise)');
          expect(device.currentGeoPosition.geopoint.lat).to.equal(networkLat); // Network Pos
          expect(device.currentGeoPosition.geopoint.lng).to.equal(networkLng); // Network Pos
          done();
        });
      }, timeoutCheck);
    });
  });

  describe('(2) v2 - No site correction', function() {
    it('with no wifi in the middle of nowhere (Network)', function(done) {
      receivedDeviceEventPos('v2', deviceIdv2, 45.284712, 1.059003, 9000, 0);
      receivedDeviceEventAttrs('v2', deviceIdv2, 0, null, 0);
      setTimeout(() => {
        app.models.Device.findById(deviceIdv2, (err, device) => {
          expect(err).to.be.null;
          expect(device).to.have.property('currentGeoPosition');
          expect(device.currentGeoPosition).to.have.property('source');
          expect(device.currentGeoPosition.source).to.equal('Network');
          done();
        });
      }, timeoutCheck);
    });

    it('with no wifi in the middle of nowhere (Wifi)', function(done) {
      receivedDeviceEventPos('v2', deviceIdv2, 45.284712, 1.059003, 9000, 0, true);
      receivedDeviceEventAttrs('v2', deviceIdv2, 0, null, 0);
      setTimeout(() => {
        app.models.Device.findById(deviceIdv2, (err, device) => {
          expect(err).to.be.null;
          expect(device).to.have.property('currentGeoPosition');
          expect(device.currentGeoPosition).to.have.property('source');
          expect(device.currentGeoPosition.source).to.equal('Wifi');
          done();
        });
      }, timeoutCheck);
    });

    it('with matching wifi in the middle of nowhere (Network)', function(done) {
      const networkLat = 43.415628, networkLng = 6.077538; // Brignoles (>90 <100kms)
      receivedDeviceEventPos('v2', deviceIdv2, networkLat, networkLng, 9000, 0);
      receivedDeviceEventAttrs('v2', deviceIdv2, 0, ['a4:5e:60:d3:cb:b3'], 0);
      setTimeout(() => {
        app.models.Device.findById(deviceIdv2, (err, device) => {
          expect(err).to.be.null;
          expect(device).to.have.property('currentGeoPosition');
          expect(device.currentGeoPosition).to.have.property('source');
          expect(device.currentGeoPosition.source).to.equal('Wifi (mylnikov.org)');
          expect(device.currentGeoPosition.geopoint.lat).to.equal(43.73210474767); // Wifi Pos
          expect(device.currentGeoPosition.geopoint.lng).to.equal(7.16753567864); // Wifi Pos
          done();
        });
      }, timeoutCheck);
    });

    it('with first non matching wifi in the middle of nowhere (Network)', function(done) {
      const networkLat = 43.415628, networkLng = 6.077538; // Brignoles (>90 <100kms)
      receivedDeviceEventPos('v2', deviceIdv2, networkLat, networkLng, 9000, 0);
      receivedDeviceEventAttrs('v2', deviceIdv2, 0, ['00:00:00:00:00:00', 'a4:5e:60:d3:cb:b3'], 0);
      setTimeout(() => {
        app.models.Device.findById(deviceIdv2, (err, device) => {
          expect(err).to.be.null;
          expect(device).to.have.property('currentGeoPosition');
          expect(device.currentGeoPosition).to.have.property('source');
          expect(device.currentGeoPosition.source).to.equal('Network');
          expect(device.currentGeoPosition.geopoint.lat).to.equal(networkLat); // Wifi Pos
          expect(device.currentGeoPosition.geopoint.lng).to.equal(networkLng); // Wifi Pos
          done();
        });
      }, timeoutCheck);
    });

    it('with no matching wifi in the middle of nowhere (Network)', function(done) {
      const networkLat = 43.834872, networkLng = 5.824149; // Brignoles (>90 <100kms)
      receivedDeviceEventPos('v2', deviceIdv2, networkLat, networkLng, 9000, 0);
      receivedDeviceEventAttrs('v2', deviceIdv2, 0, ['a4:5e:60:d3:cb:b3'], 0);
      setTimeout(() => {
        app.models.Device.findById(deviceIdv2, (err, device) => {
          expect(err).to.be.null;
          expect(device).to.have.property('currentGeoPosition');
          expect(device.currentGeoPosition).to.have.property('source');
          expect(device.currentGeoPosition.source).to.equal('Network (wifi mylnikov.org was invalid or less precise)');
          expect(device.currentGeoPosition.geopoint.lat).to.equal(networkLat); // Network Pos
          expect(device.currentGeoPosition.geopoint.lng).to.equal(networkLng); // Network Pos
          done();
        });
      }, timeoutCheck);
    });
  });
  describe('(3) V1 - PSA Site matching', function() {
    beforeEach(done => {
      app.models.GeoPosition.destroyAll({'deviceId': deviceIdv1}, done);
    });
    it('with MAC@ at PSA in an off-traffic site', function(done) {
      const networkLat = 43.834872, networkLng = 5.824149; // Brignoles (>90 <100kms)
      receivedDeviceEventPos('v1', deviceIdv1, networkLat, networkLng, 9000, 0);
      receivedDeviceEventAttrs('v1', deviceIdv1, 0, '00:3a:7d:eb:b9:00', 0);
      setTimeout(() => {
        app.models.Device.findById(deviceIdv1, (err, device) => {
          expect(err).to.be.null;
          expect(device).to.have.property('currentGeoPosition');
          expect(device.currentGeoPosition).to.have.property('source');
          expect(device.currentGeoPosition.source).to.equal('Wifi (Site resolution)');
          expect(device.currentGeoPosition).to.not.have.ownPropertyDescriptor('siteId');
          done();
        });
      }, timeoutCheck);
    });
    it('with MAC@ at PSA in an on-traffic site', function(done) {
      const networkLat = 43.834872, networkLng = 5.824149; // Brignoles (>90 <100kms)
      receivedDeviceEventPos('v1', deviceIdv1, networkLat, networkLng, 9000, 0);
      receivedDeviceEventAttrs('v1', deviceIdv1, 0, '3c:0e:23:c8:41:00', 0);
      setTimeout(() => {
        app.models.Device.findById(deviceIdv1, (err, device) => {
          expect(err).to.be.null;
          expect(device).to.have.property('currentGeoPosition');
          expect(device.currentGeoPosition).to.have.property('source');
          expect(device.currentGeoPosition.source).to.equal('Wifi (Site resolution)');
          expect(device.currentGeoPosition).to.have.property('siteId');
          expect(device.currentGeoPosition.siteId).to.equal('PSA Sochaux');
          done();
        });
      }, timeoutCheck);
    });
  });
});

function startServer(cb) {
  console.log('>> Starting server');
  server = app.listen(() => {
    iotWorker.start();
    return cb();
  });
}

function stopServer(cb) {
  console.log('<< Stopping server');
  return server.close(cb);
}

function createSigfoxPosPayloadBufferV1(deviceId, lat, lng, radius, seqNumber) {
  const payload = {
    time: Date.now() / 1000,
    deviceType: 'test-device',
    device: deviceId,
    duplicate: 'false',
    snr: '20.00',
    rssi: '-131.00',
    avgSnr: '20.00',
    station: '0001',
    lat: '' + lat,
    lng: '' + lng,
    radius: '' + radius,
    seqNumber: '' + seqNumber
  };
  return Buffer.from(JSON.stringify(payload), 'utf8');
}

function createSigfoxPosPayloadBufferV2(deviceId, lat, lng, radius, seqNumber, isWifiSource) {
  let payload;
  if (isWifiSource) {
    payload = {
      device: deviceId,
      seqNumber: seqNumber,
      time: Date.now() / 1000,
      lat: lat,
      lng: lng,
      radius: radius,
      source: 'Wifi'
    };
  } else {
    payload = {
      device: deviceId,
      seqNumber: seqNumber,
      time: Date.now() / 1000,
      lat: lat,
      lng: lng,
      radius: radius,
      source: 'Network'
    };
  }
  return Buffer.from(JSON.stringify(payload), 'utf8');
}

function createSigfoxAttrsPayloadBufferV1(deviceId, batteryLevel, macAddress, seqNumber) {
  const payload = {
    time: Date.now() / 1000,
    deviceType: 'test-device',
    device: deviceId,
    duplicate: 'false',
    snr: '20.00',
    rssi: '-131.00',
    avgSnr: '20.00',
    station: '0001',
    lat: '47.0',
    lng: '7.0',
    data: '000000' + (macAddress && macAddress.length === 17 ? macAddress.replace(/\:/g, '') : '000000000000'),
    seqNumber: '' + seqNumber
  };
  return Buffer.from(JSON.stringify(payload), 'utf8');
}

function createSigfoxAttrsPayloadBufferV2(deviceId, batteryLevel, macAddresses, seqNumber) {
  const payload = {
    time: Date.now() / 1000,
    deviceType: 'test-device',
    device: deviceId,
    duplicate: 'false',
    snr: '20.00',
    rssi: '-131.00',
    avgSnr: '20.00',
    station: '0001',
    lat: '47.0',
    lng: '7.0',
    data:
    (macAddresses && Array.isArray(macAddresses) && macAddresses[0] && macAddresses[0].length === 17 ? macAddresses[0].replace(/\:/g, '') : '000000000000') +
    (macAddresses && Array.isArray(macAddresses) && macAddresses[1] && macAddresses[1].length === 17 ? macAddresses[1].replace(/\:/g, '') : '000000000000'),
    seqNumber: '' + seqNumber
  };
  return Buffer.from(JSON.stringify(payload), 'utf8');
}

function receivedDeviceEventPos(version, deviceId, lat, lng, radius, seqNumber, isWifiPos) {
  if (version === 'v1') {
    iotWorker.receivedDeviceEvent('sigfox-auth-device', deviceId, '', '', createSigfoxPosPayloadBufferV1(deviceId, lat, lng, radius, seqNumber));
  } else {
    iotWorker.receivedDeviceEvent('sigfox-auth-device-v2', deviceId, '', '', createSigfoxPosPayloadBufferV2(deviceId, lat, lng, radius, seqNumber, isWifiPos));
  }
}

function receivedDeviceEventAttrs(version, deviceId, batteryLevel, macAddress, seqNumber) {
  if (version === 'v1') {
    iotWorker.receivedDeviceEvent('sigfox-auth-device', deviceId, '', '', createSigfoxAttrsPayloadBufferV1(deviceId, batteryLevel, macAddress, seqNumber));
  } else {
    iotWorker.receivedDeviceEvent('sigfox-auth-device-v2', deviceId, '', '', createSigfoxAttrsPayloadBufferV2(deviceId, batteryLevel, macAddress, seqNumber));
  }
}
