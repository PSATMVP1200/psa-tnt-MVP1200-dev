'use strict';

const _ = require('underscore'),
  app = require('../server/server'),
  wifiLocator = require('../common/lib/wifi-locator'),
  async = require('async'),
  chai = require('chai'),
  expect = chai.expect,
  moment = require('moment');

chai.use(require('chai-string'));

let server, Container, Device;

xdescribe('Wifi MACs sites', function() {
  this.timeout(5000);
  before(startServer);
  after(stopServer);

  describe('Test with crap', function() {
    it('array with crap', function(done) {
      app.models.Site.findByWifiMac(['', null, {}, 1], (err, resp) => {
        expect(err).to.be.null;
        expect(resp).to.be.undefined;
        done();
      });
    });
    it('pure crap #1', function(done) {
      app.models.Site.findByWifiMac(null, (err, resp) => {
        expect(err).to.be.null;
        expect(resp).to.be.undefined;
        done();
      });
    });
    it('pure crap #2', function(done) {
      app.models.Site.findByWifiMac('', (err, resp) => {
        expect(err).to.be.null;
        expect(resp).to.be.undefined;
        done();
      });
    });
    it('pure crap #3', function(done) {
      app.models.Site.findByWifiMac({}, (err, resp) => {
        expect(err).to.be.null;
        expect(resp).to.be.undefined;
        done();
      });
    });
  });
  describe('Test with wifi macs', function() {
    it('invalid macs', function(done) {
      app.models.Site.findByWifiMac(['00:00:00:00:00:00', '00:00:00:00:00:01'], (err, resp) => {
        expect(err).to.be.null;
        expect(resp).to.be.undefined;
        done();
      });
    });
  });
  describe('Test with valid wifi macs', function() {
    it('one exact matching mac', function(done) {
      app.models.Site.findByWifiMac(['00:3a:7d:eb:b9:e0'], (err, resp) => {
        expect(err).to.be.null;
        expect(resp).to.be.an('object').that.is.not.empty;
        expect(resp).to.have.property('name');
        done();
      });
    });
    it('one exact matching mac and two not matching', function(done) {
      app.models.Site.findByWifiMac(['00:00:00:00:00:00', '00:3a:7d:eb:b9:e0', '00:00:00:00:00:00'], (err, resp) => {
        expect(err).to.be.null;
        expect(resp).to.be.an('object').that.is.not.empty;
        expect(resp).to.have.property('name');
        done();
      });
    });
    it('one approx. matching', function(done) {
      app.models.Site.findByWifiMac(['00:3a:7d:eb:b9:00'], (err, resp) => {
        expect(err).to.be.null;
        expect(resp).to.be.an('object').that.is.not.empty;
        expect(resp).to.have.property('name');
        done();
      });
    });
    it('one approx. not matching', function(done) {
      app.models.Site.findByWifiMac(['00:3a:7d:eb:ba:00'], (err, resp) => {
        expect(err).to.be.null;
        expect(resp).to.be.an('object').that.is.not.empty;
        expect(resp).to.have.property('name');
        done();
      });
    });
  });
});

function startServer(cb) {
  console.log('>> Starting server');
  server = app.listen(() => {
    Device = app.models.Device;
    Container = app.models.Container;
    return cb();
  });
}

function stopServer(cb) {
  console.log('<< Stopping server');
  return server.close(cb);
}
