'use strict';

const _ = require('underscore'),
  app = require('../server/server'),
  async = require('async'),
  chai = require('chai'),
  expect = chai.expect,
  moment = require('moment');

chai.use(require('chai-string'));

let server, Container, Device;

const now = new Date((new Date()).setSeconds(0));

describe('Containers status tests', function() {
  this.timeout(5000);
  before(startServer);
  after(stopServer);

  describe('(1) Test status with no lead times', function() {
    it('(1) 10811.1 created off-site', function(done) {
      const deviceId = '10811.1';
      test({
        'type': 'create',
        'deviceId': deviceId,
        'containerType': '10811',
        'lat': 47.509509,
        'lng': 9.812390
      }, {
        'currentStatusMarker': 'blue',
        'currentStatusDesc': 'In Traffic',
        'currentGeoPositionSite': undefined,
        'currentSiteInTraffic': undefined,
        'lastSiteInTraffic': undefined,
        'beforeLastSiteInTraffic': undefined
      }, done);
    });

    it('(2) 10811.2 created on on-traffic-site (Reydel Rougegoutte)', function(done) {
      const deviceId = '10811.2';
      test({
        'type': 'create',
        'deviceId': deviceId,
        'containerType': '10811',
        'lat': 47.729505,
        'lng': 6.846997
      }, {
        'currentStatusMarker': 'green',
        'currentStatusDesc': 'OK',
        'currentGeoPositionSite': 'REYDEL Rougegoutte',
        'currentSiteInTraffic': 'REYDEL Rougegoutte',
        'lastSiteInTraffic': undefined,
        'beforeLastSiteInTraffic': undefined
      }, done);
    });

    it('(3) 10811.3 created on off-traffic-site (PSA Mulhouse but not in-traffic)', function(done) {
      const deviceId = '10811.3';
      test({
        'type': 'create',
        'deviceId': deviceId,
        'containerType': '10811',
        'lat': 47.778267,
        'lng': 7.436249
      }, {
        'currentStatusMarker': 'red',
        'currentStatusDesc': 'Site not in traffic',
        'currentGeoPositionSite': 'PSA Mulhouse',
        'currentSiteInTraffic': undefined,
        'lastSiteInTraffic': undefined,
        'beforeLastSiteInTraffic': undefined
      }, done);
    });

    it('(4) 10811.1 off site > on-traffic-site ', done => {
      const deviceId = '10811.1';
      test({
        'type': 'move',
        'deviceId': deviceId,
        'lat': 47.509509,
        'lng': 6.812390
      }, {
        'currentStatusMarker': 'green',
        'currentStatusDesc': 'OK',
        'currentGeoPositionSite': 'PSA Sochaux',
        'currentSiteInTraffic': 'PSA Sochaux',
        'lastSiteInTraffic': undefined,
        'beforeLastSiteInTraffic': undefined
      }, done);
    });

    it('(5) 10811.1 on-traffic-site > off site', done => {
      const deviceId = '10811.1';
      test({
        'type': 'move',
        'deviceId': deviceId,
        'lat': 47.509509,
        'lng': 7.812390
      }, {
        'currentStatusMarker': 'blue',
        'currentStatusDesc': 'In Traffic',
        'currentGeoPositionSite': undefined,
        'currentSiteInTraffic': null,
        'lastSiteInTraffic': 'PSA Sochaux',
        'beforeLastSiteInTraffic': undefined
      }, done);
    });

    it('(6) 10811.1 off site > off-traffic-site (PSA Mulhouse but not in-traffic)', function(done) {
      const deviceId = '10811.1';
      test({
        'type': 'move',
        'deviceId': deviceId,
        'lat': 47.778267,
        'lng': 7.436249
      }, {
        'currentStatusMarker': 'red',
        'currentStatusDesc': 'Site not in traffic',
        'currentGeoPositionSite': 'PSA Mulhouse',
        'currentSiteInTraffic': null,
        'lastSiteInTraffic': 'PSA Sochaux',
        'beforeLastSiteInTraffic': undefined
      }, done);
    });

    it('(7) 10811.1 off site > on-traffic-site (Reydel Rougegoutte)', function(done) {
      const deviceId = '10811.1';
      test({
        'type': 'move',
        'deviceId': deviceId,
        'lat': 47.729505,
        'lng': 6.846997
      }, {
        'currentStatusMarker': 'green',
        'currentStatusDesc': 'OK',
        'currentGeoPositionSite': 'REYDEL Rougegoutte',
        'currentSiteInTraffic': 'REYDEL Rougegoutte',
        'lastSiteInTraffic': 'PSA Sochaux',
        'beforeLastSiteInTraffic': undefined
      }, done);
    });

    it('(8) 10811.1 on site > off site', function(done) {
      const deviceId = '10811.1';
      test({
        'type': 'move',
        'deviceId': deviceId,
        'lat': 47.509509,
        'lng': 7.812390
      }, {
        'currentStatusMarker': 'blue',
        'currentStatusDesc': 'In Traffic',
        'currentGeoPositionSite': undefined,
        'currentSiteInTraffic': null,
        'lastSiteInTraffic': 'REYDEL Rougegoutte',
        'beforeLastSiteInTraffic': 'PSA Sochaux'
      }, done);
    });
  });

  describe('(2) Test status with lead times', function() {
    it('(1) 10811.4 created on on-traffic-site (Reydel Rougegoutte)', function(done) {
      const deviceId = '10811.4';
      test({
        'type': 'create',
        'deviceId': deviceId,
        'containerType': '10811',
        'lat': 47.729505,
        'lng': 6.846997,
        'date': now
      }, {
        'currentStatusMarker': 'green',
        'currentStatusDesc': 'OK',
        'currentGeoPositionSite': 'REYDEL Rougegoutte',
        'currentSiteInTraffic': 'REYDEL Rougegoutte',
        'lastSiteInTraffic': undefined,
        'beforeLastSiteInTraffic': undefined
      }, done);
    });

    it('(2) 10811.4 still on on-traffic-site (Reydel Rougegoutte) after 3 hours', function(done) {
      const threeHoursLater = new Date(now.getTime() + (3 * 60000));
      threeHoursLater.setSeconds(0);
      const deviceId = '10811.4';
      test({
        'type': 'move',
        'deviceId': deviceId,
        'lat': 47.729505,
        'lng': 6.846997,
        'date': threeHoursLater
      }, {
        'currentStatusMarker': 'green',
        'currentStatusDesc': 'OK',
        'currentGeoPositionSite': 'REYDEL Rougegoutte',
        'currentSiteInTraffic': 'REYDEL Rougegoutte',
        'lastSiteInTraffic': undefined,
        'beforeLastSiteInTraffic': undefined
      }, done);
    });

    it('(3) 10803.1 created on-traffic-site (Faurecia Marckolsheim)', function(done) {
      const deviceId = '10803.1';
      test({
        'type': 'create',
        'containerType': '10803',
        'deviceId': deviceId,
        'lat': 48.154479,
        'lng': 7.552821,
        'date': now
      }, {
        'currentStatusMarker': 'green',
        'currentStatusDesc': 'OK',
        'currentGeoPositionSite': 'Faurecia Marckolsheim',
        'currentSiteInTraffic': 'Faurecia Marckolsheim',
        'lastSiteInTraffic': undefined,
        'beforeLastSiteInTraffic': undefined
      }, done);
    });

    it('(4) 10803.1 on-traffic-site > off-traffic-site 1442 minutes later (PSA Mulhouse)', function(done) {
      const date = new Date(now.getTime() + (1442 * 60000));
      date.setSeconds(0);
      const deviceId = '10803.1';
      test({
        'type': 'move',
        'deviceId': deviceId,
        'lat': 47.778267,
        'lng': 7.436249,
        'date': date
      }, {
        'currentStatusMarker': 'red',
        'currentStatusDesc': 'Site not in traffic',
        'currentGeoPositionSite': 'PSA Mulhouse',
        'currentSiteInTraffic': null,
        'lastSiteInTraffic': 'Faurecia Marckolsheim',
        'beforeLastSiteInTraffic': undefined
      }, done);
    });

    it('(5) 10803.1 still on traffic 2162 minutes later (PSA Mulhouse)', function(done) {
      const date = new Date(now.getTime() + (2162 * 60000));
      date.setSeconds(0);
      const dueDate = new Date(now.getTime() + (2160 * 60000));
      dueDate.setSeconds(0);
      const deviceId = '10803.1';
      test({
        'type': 'move',
        'deviceId': deviceId,
        'lat': 47.778267,
        'lng': 7.436249,
        'date': date
      }, {
        'currentStatusMarker': 'red',
        //'currentStatusDesc': 'Late (Due : ' + toDate(dueDate) + ')',
        'currentGeoPositionSite': 'PSA Mulhouse',
        'currentSiteInTraffic': null,
        'lastSiteInTraffic': 'Faurecia Marckolsheim',
        'beforeLastSiteInTraffic': undefined
      }, done);
    });

    it('(6) 10803.1 finally arrived at PSA Sochaux 2163 minutes later', function(done) {
      const date = new Date(now.getTime() + (2163 * 60000));
      date.setSeconds(0);
      const dueDate = new Date(now.getTime() + (1440 * 60000));
      dueDate.setSeconds(0);
      const deviceId = '10803.1';
      test({
        'type': 'move',
        'deviceId': deviceId,
        'lat': 47.509509,
        'lng': 6.812390,
        'date': date
      }, {
        'currentStatusMarker': 'orange',
        //'currentStatusDesc': 'Late (Due : ' + toDate(dueDate) + ')',
        'currentGeoPositionSite': 'PSA Sochaux',
        'currentSiteInTraffic': 'PSA Sochaux',
        'lastSiteInTraffic': 'Faurecia Marckolsheim',
        'beforeLastSiteInTraffic': undefined
      }, done);
    });
  });

  describe('(4) Test reset', function() {
    const deviceId = '10803.R';

    it('(1) 10803.R created on on-traffic-site (Marckolsheim)', function(done) {
      test({
        'type': 'create',
        'deviceId': deviceId,
        'containerType': '10803',
        'lat': 48.154479,
        'lng': 7.552821
      }, {
        'currentStatusMarker': 'green',
        'currentStatusDesc': 'OK',
        'currentGeoPositionSite': 'Faurecia Marckolsheim',
        'currentSiteInTraffic': 'Faurecia Marckolsheim',
        'lastSiteInTraffic': undefined,
        'beforeLastSiteInTraffic': undefined
      }, done);
    });

    it('(2) 10803.R moves to Bains-sur-Oust', function(done) {
      test({
        'type': 'move',
        'deviceId': deviceId,
        'lat': 47.686662,
        'lng': -2.055756
      }, {
        'currentStatusMarker': 'blue',
        'currentStatusDesc': 'In Traffic',
        'currentGeoPositionSite': 'Faurecia Bains-sur-Oust',
        'currentSiteInTraffic': null,
        'lastSiteInTraffic': 'Faurecia Marckolsheim',
        'beforeLastSiteInTraffic': undefined
      }, done);
    });

    it('(3) 10803.R is resetted', done => {
      Device.findById(deviceId, (err, device) => {
        if (err) {
          return done(err);
        }
        device.reset(err => {
          if (err) {
            return done(err);
          }
          expect(device.currentGeoPosition.isReset).to.be.true;
          return done();
        });
      });
    });

    it('(4) resetted 10803.1 still in Rennes', function(done) {
      test({
        'type': 'move',
        'deviceId': deviceId,
        'lat': 47.686662,
        'lng': -2.055756
      }, {
        'currentStatusMarker': 'green',
        'currentStatusDesc': 'OK',
        'currentGeoPositionSite': 'Faurecia Bains-sur-Oust',
        'currentSiteInTraffic': 'Faurecia Bains-sur-Oust',
        'lastSiteInTraffic': 'Faurecia Marckolsheim'
      }, (err) => {
        if (err) {
          return done(err);
        }
        Device.findById(deviceId, (err, device) => {
          if (err) {
            return done(err);
          }
          expect(device.currentGeoPosition.isReset).to.be.undefined;
          return done();
        });
      });
    });
  });

  describe('(5) Test static', function() {
    it('(1) 10035.1 off site', function(done) {
      const deviceId = '10035.1';
      test({
        'type': 'create',
        'containerType': '10035',
        'deviceId': deviceId,
        'lat': 48.539958,
        'lng': 1.293813,
        'date': now
      }, {
        'currentStatusMarker': 'blue',
        'currentStatusDesc': 'In Traffic',
        'currentGeoPositionSite': undefined,
        'currentSiteInTraffic': undefined,
        'lastSiteInTraffic': undefined,
        'beforeLastSiteInTraffic': undefined
      }, (err) => {
        if (err) {
          return done(err);
        }
        Device.findById(deviceId, (err, device) => {
          if (err) {
            return done(err);
          }
          expect(device.currentGeoPosition.isReset).to.be.undefined;
          return done();
        });
      });
    });

    it('(2) 10035.1 still off site 47 hours 59 mins ', function(done) {
      const later = new Date(now.getTime() + (2879 * 60000));
      later.setSeconds(0);
      const deviceId = '10035.1';
      test({
        'type': 'move',
        'deviceId': deviceId,
        'lat': 48.539958,
        'lng': 1.293813,
        'date': later
      }, {
        'currentStatusMarker': 'blue',
        'currentStatusDesc': 'In Traffic',
        'currentGeoPositionSite': undefined,
        'currentSiteInTraffic': undefined,
        'lastSiteInTraffic': undefined,
        'beforeLastSiteInTraffic': undefined
      }, (err) => {
        if (err) {
          return done(err);
        }
        Device.findById(deviceId, (err, device) => {
          if (err) {
            return done(err);
          }
          expect(device.currentGeoPosition.isReset).to.be.undefined;
          return done();
        });
      });
    });

    it('(3) 10035.1 still off site 48 hours 2 mins ago', function(done) {
      const later = new Date(now.getTime() + (2882 * 60000));
      later.setSeconds(0);
      const deviceId = '10035.1';
      test({
        'type': 'move',
        'deviceId': deviceId,
        'lat': 48.539958,
        'lng': 1.293813,
        'date': later
      }, {
        'currentStatusMarker': 'red',
        //'currentStatusDesc': 'Static since ' + toDate(now),
        'currentGeoPositionSite': undefined,
        'currentSiteInTraffic': undefined,
        'lastSiteInTraffic': undefined,
        'beforeLastSiteInTraffic': undefined
      }, (err) => {
        if (err) {
          return done(err);
        }
        Device.findById(deviceId, (err, device) => {
          if (err) {
            return done(err);
          }
          expect(device.currentGeoPosition.isReset).to.be.undefined;
          return done();
        });
      });
    });

    it('(4) 10035.1 still off site 60 hours after', function(done) {
      const later = new Date(now.getTime() + (60 * 60 * 60000));
      later.setSeconds(0);
      const deviceId = '10035.1';
      test({
        'type': 'move',
        'deviceId': deviceId,
        'lat': 48.539958,
        'lng': 1.293813,
        'date': later
      }, {
        'currentStatusMarker': 'red',
        //'currentStatusDesc': 'Static since ' + toDate(now),
        'currentGeoPositionSite': undefined,
        'currentSiteInTraffic': undefined,
        'lastSiteInTraffic': undefined,
        'beforeLastSiteInTraffic': undefined
      }, (err) => {
        if (err) {
          return done(err);
        }
        Device.findById(deviceId, (err, device) => {
          if (err) {
            return done(err);
          }
          expect(device.currentGeoPosition.isReset).to.be.undefined;
          return done();
        });
      });
    });
  });

  // TODO Add quiet tests

  describe('(6) Test v2', function() {
    it('(1) 10811.5 created on on-traffic-site WiFi (PSA Sochaux)', function(done) {
      const deviceId = '10811.5';
      test({
        'type': 'create',
        'deviceId': deviceId,
        'containerType': '10811',
        'lat': 47.509509,
        'lng': 6.812390,
        'date': now
      }, {
        'currentStatusMarker': 'green',
        'currentStatusDesc': 'OK',
        'currentGeoPositionSite': 'PSA Sochaux',
        'currentSiteInTraffic': 'PSA Sochaux',
        'lastSiteInTraffic': undefined,
        'beforeLastSiteInTraffic': undefined
      }, done);
    });

    it('(2) 10811.5 moved to Rougegotte within lead time', function(done) {
      const later = new Date(Date.now() + (119 * 60000));
      later.setSeconds(0);
      const deviceId = '10811.5';
      test({
        'type': 'move',
        'deviceId': deviceId,
        'lat': 47.729505,
        'lng': 6.846997,
        'date': later
      }, {
        'currentStatusMarker': 'green',
        'currentStatusDesc': 'OK',
        'currentGeoPositionSite': 'REYDEL Rougegoutte',
        'currentSiteInTraffic': 'REYDEL Rougegoutte',
        'lastSiteInTraffic': 'PSA Sochaux',
        'beforeLastSiteInTraffic': undefined
      }, done);
    });

    it('(3) 10811.5 stays in Rougegotte after lead time', function(done) {
      const date = new Date(Date.now() + (2878 * 60000));
      date.setSeconds(0);
      const deviceId = '10811.5';
      test({
        'type': 'move',
        'deviceId': deviceId,
        'lat': 47.729505,
        'lng': 6.846997,
        'date': date
      }, {
        'currentStatusMarker': 'green',
        'currentStatusDesc': 'OK',
        'currentGeoPositionSite': 'REYDEL Rougegoutte',
        'currentSiteInTraffic': 'REYDEL Rougegoutte',
        'lastSiteInTraffic': 'PSA Sochaux',
        'beforeLastSiteInTraffic': undefined
      }, done);
    });

    it('(4) Leaves REYDEL after almost 48 hours', function(done) {
      const date = new Date(Date.now() + (2878 * 60000));
      date.setSeconds(0);
      const deviceId = '10811.5';
      test({
        'type': 'move',
        'deviceId': deviceId,
        'lat': 47.741994,
        'lng': 6.799119,
        'date': date
      }, {
        'currentStatusMarker': 'blue',
        'currentStatusDesc': 'In Traffic',
        'currentGeoPositionSite': undefined,
        'currentSiteInTraffic': null,
        'lastSiteInTraffic': 'REYDEL Rougegoutte',
        'beforeLastSiteInTraffic': 'PSA Sochaux'
      }, done);
    });
    // Static within lead time
  });
  describe('(7) Container comes back on same site', function() {
    const deviceId = '10811.7';
    it('(1) 10811.7 created at PSA Sochaux', function(done) {
      const date = new Date(Date.now());
      date.setSeconds(0);
      test({
        'type': 'create',
        'containerType': '10811',
        'deviceId': deviceId,
        'lat': 47.509509,
        'lng': 6.812390,
        'date': date
      }, {
        'currentStatusMarker': 'green',
        'currentStatusDesc': 'OK',
        'currentGeoPositionSite': 'PSA Sochaux',
        'currentSiteInTraffic': 'PSA Sochaux',
        'lastSiteInTraffic': undefined,
        'beforeLastSiteInTraffic': undefined
      }, done);
    });
    it('(2) Moves out of PSA Sochaux 1 hour later', function(done) {
      const date = new Date(Date.now() + 1 * 1000 * 60 * 60);
      date.setSeconds(0);
      test({
        'type': 'move',
        'deviceId': deviceId,
        'lat': 47.472341,
        'lng': 6.509664,
        'date': date
      }, {
        'currentStatusMarker': 'blue',
        'currentStatusDesc': 'In Traffic',
        'currentGeoPositionSite': undefined,
        'currentSiteInTraffic': null,
        'lastSiteInTraffic': 'PSA Sochaux',
        'beforeLastSiteInTraffic': undefined
      }, done);
    });
    it('(3) Comes back in PSA Sochaux 1h30 mins later', function(done) {
      const date = new Date(Date.now() + 1.5 * 1000 * 60 * 60);
      date.setSeconds(0);
      test({
        'type': 'move',
        'deviceId': deviceId,
        'lat': 47.509509,
        'lng': 6.812390,
        'date': date
      }, {
        'currentStatusMarker': 'green',
        'currentStatusDesc': 'OK',
        'currentGeoPositionSite': 'PSA Sochaux',
        'currentSiteInTraffic': 'PSA Sochaux',
        'lastSiteInTraffic': 'PSA Sochaux',
        'beforeLastSiteInTraffic': undefined
      }, done);
    });
  });
});

function createAndPositionDeviceAndContainer(extParams, cb) {
  async.waterfall([
    cb => {
      Device.create({'id': extParams.deviceId, 'type': 'sigfox-auth-device'}, cb);
    },
    (_device, cb) => {
      _device.container.create({'containerTypeId': extParams.containerType}, cb);
    },
    (_container, cb) => {
      Device.submitEvent(cleanParams(extParams), cb);
    }
  ], (err) => {
    if (err) {
      return cb(err);
    }
    return moveContainer(extParams, cb);
  });
}

function moveContainer(extParams, cb) {
  let device, container;
  async.series([
    cb => {
      Device.submitEvent(cleanParams(extParams), cb);
    },
    cb => {
      Device.findById(extParams.deviceId, (err, _device) => {
        device = _device;
        if (err) {
          return cb(err);
        }
        device.container((err, _container) => {
          if (err) {
            return cb(err);
          }
          container = _container;
          return cb(null);
        });
      });
    }
  ], (err) => {
    if (err) {
      return cb(err);
    }
    return cb(null, device, container);
  });
}

function startServer(cb) {
  console.log('>> Starting server');
  server = app.listen(() => {
    Device = app.models.Device;
    Container = app.models.Container;
    return cb();
  });
}

function stopServer(cb) {
  console.log('<< Stopping server');
  return server.close(cb);
}

function test(event, expected, done) {
  if (event.type === 'create') {
    createAndPositionDeviceAndContainer(
      _.omit(event, 'type'),
      (err, device, container) => {
        if (err) {
          return done(err);
        }
        validate(device, container, expected);
        return done();
      });
  } else if (event.type === 'move') {
    moveContainer(
      _.omit(event, 'type'),
      (err, device, container) => {
        if (err) {
          return done(err);
        }
        validate(device, container, expected);
        return done();
      });
  } else {
    return done(new Error('Unexpected type: ' + event.type));
  }
}

function validate(device, container, expected) {
  if (expected.hasOwnProperty('currentStatusMarker')) {
    expect(container.currentStatus.marker).to.be.equal(expected.currentStatusMarker);
  }
  if (expected.hasOwnProperty('currentStatusDesc')) {
    expect(container.currentStatus.description).to.be.equal(expected.currentStatusDesc);
  }
  if (expected.hasOwnProperty('currentGeoPositionSite')) {
    expect(device.currentGeoPosition.siteId).to.be.equal(expected.currentGeoPositionSite);
  }
  if (expected.hasOwnProperty('currentSiteInTraffic')) {
    expect(device.currentSiteInTrafficId).to.be.equal(expected.currentSiteInTraffic);
  }
  if (expected.hasOwnProperty('lastSiteInTraffic')) {
    expect(device.lastSiteInTrafficId).to.be.equal(expected.lastSiteInTraffic);
  }
  if (expected.hasOwnProperty('beforeLastSiteInTraffic')) {
    expect(device.beforeLastSiteInTrafficId).to.be.equal(expected.beforeLastSiteInTraffic);
  }
}

function cleanParams(extParams) {
  return _.pick(extParams, 'deviceId', 'lat', 'lng', 'wifiMac', 'batteryLevel', 'date');
}

function toDate(date) {
  if (!moment(new Date(), 'Europe/Paris').isDST()) {
    date = new Date(date.getTime() - 60 * 60 * 1000);
  }
  return moment(date).format('DD/MM/YYYY HH:mm');
}
