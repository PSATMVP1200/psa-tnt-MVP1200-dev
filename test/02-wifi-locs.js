'use strict';

const _ = require('underscore'),
  app = require('../server/server'),
  wifiLocator = require('../common/lib/wifi-locator'),
  async = require('async'),
  chai = require('chai'),
  expect = chai.expect,
  moment = require('moment');

chai.use(require('chai-string'));

let server, Container, Device;

const now = new Date((new Date()).setSeconds(0));

xdescribe('Containers status tests', function() {
  this.timeout(5000);
  before(startServer);
  after(stopServer);

  describe('(0) Mac is a neither a string nor an array', function() {
    it(': null', function(done) {
      wifiLocator.singleWifiGeoLoc(null, (err, val) => {
        expect(err).to.not.be.null;
        done();
      });
    });
    it(': {}', function(done) {
      wifiLocator.singleWifiGeoLoc({}, (err, val) => {
        expect(err).to.not.be.null;
        done();
      });
    });
  });

  describe('(1) Mac is a string', function() {
    it('that is a valid mac', function(done) {
      wifiLocator.singleWifiGeoLoc('A0:F3:C1:3B:6F:90', (err, val) => {
        expect(err).to.be.null;
        expect(val).to.not.be.null;
        expect(val).to.have.property('geopoint');
        done();
      });
    });
    it('that is an invalid mac', function(done) {
      wifiLocator.singleWifiGeoLoc('0:F3:C1:3B:6F:90', (err, val) => {
        expect(err).to.be.null;
        expect(val).to.be.null;
        done();
      });
    });
  });

  describe('(2) Mac is an array of one string', function() {
    it('that is a valid mac', function(done) {
      wifiLocator.singleWifiGeoLoc(['A0:F3:C1:3B:6F:90'], (err, val) => {
        expect(err).to.be.null;
        expect(val).to.not.be.null;
        expect(val).to.have.property('geopoint');
        done();
      });
    });
    it('that is an invalid mac', function(done) {
      wifiLocator.singleWifiGeoLoc(['0:F3:C1:3B:6F:90'], (err, val) => {
        expect(err).to.be.null;
        expect(val).to.be.null;
        done();
      });
    });
    it('that is crap mac', function(done) {
      wifiLocator.singleWifiGeoLoc([{}], (err, val) => {
        expect(err).to.be.null;
        expect(val).to.be.null;
        done();
      });
    });
  });

  describe('(3) Mac is an array of more than one string', function() {
    it('that contains crap and one valid mac', function(done) {
      wifiLocator.singleWifiGeoLoc(['', {}, null, 'A0:F3:C1:3B:6F:90', '0:F3:C1:3B:6F:90', '', {}, null], (err, val) => {
        expect(err).to.be.null;
        expect(val).to.not.be.null;
        expect(val).to.have.property('geopoint');
        done();
      });
    });
    it('that contains crap only', function(done) {
      wifiLocator.singleWifiGeoLoc(['', {}, null, 'AX0:F3:C1:3B:6F:90', '0:F3:C1:3B:6F:90', '', {}, null], (err, val) => {
        expect(err).to.be.null;
        expect(val).to.be.null;
        done();
      });
    });
  });
});

function startServer(cb) {
  console.log('>> Starting server');
  server = app.listen(() => {
    Device = app.models.Device;
    Container = app.models.Container;
    return cb();
  });
}

function stopServer(cb) {
  console.log('<< Stopping server');
  return server.close(cb);
}
